package charts;

/**
 * Created by Emil on 26.09.2016.
 */

// represents user-specified copy of some chartFunction
public class UserFunction extends ChartFunctionBase {
    protected final AbstractChartFunction userFunc;
    protected final String newName;


    public UserFunction(AbstractChartFunction func, String newName) {
        this.userFunc = func.clone();
        this.newName = newName;
    }


    @Override
    public String getFormula() {
        return userFunc.getFormula();
    }

    @Override
    public String getCommonFormula() {
        return userFunc.getCommonFormula();
    }

    @Override
    public String getName() {
        return this.newName;
    }

    @Override
    public double getFunctionResult(double x) {
        return userFunc.getFunctionResult(x);
    }

    @Override
    public Parameter[] getParameters() {
        return userFunc.getParameters();
    }

    @Override
    public AbstractChartFunction clone() {
        return new UserFunction(userFunc, getName());
    }
}

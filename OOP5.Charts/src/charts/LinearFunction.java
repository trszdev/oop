package charts;

/**
 * Created by Emil on 22.09.2016.
 */
public class LinearFunction extends ChartFunctionBase {
    protected Parameter a;
    protected Parameter b;

    public LinearFunction(Parameter a, Parameter b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public String getFormula() {
        return String.format("%sx %s", formatSignedDouble(a.getValue()), formatDouble(b.getValue()));
    }

    @Override
    public String getCommonFormula() {
        return "Ax + B";
    }

    @Override
    public String getName() {
        return "Linear";
    }

    @Override
    public double getFunctionResult(double x) {
        return x * a.getValue() + b.getValue();
    }

    @Override
    public Parameter[] getParameters() {
        return new Parameter[]{a, b};
    }

    @Override
    public AbstractChartFunction clone() {
        return new LinearFunction(a.clone(), b.clone());
    }
}

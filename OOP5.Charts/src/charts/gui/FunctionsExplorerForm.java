package charts.gui;

import charts.AbstractChartDrawer;
import charts.AbstractChartFunction;
import charts.Parameter;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Emil on 20.09.2016.
 */
public class FunctionsExplorerForm extends FormBase {
    protected JLabel chart = new JLabel();
    protected DefaultListModel<AbstractChartFunction> functionList =
            new DefaultListModel<>();
    protected AbstractChartFunction currentFunction;
    protected JPanel parametersPanel = new JPanel(new FlowLayout());
    JButton saveButton = factory.createButton("Save to list", e ->
            new SaveChartForm(currentFunction, functionList).setVisible(true));
    private List<JFrame> openedCharts = new ArrayList<>();
    JButton openButton = factory.createButton("Open chart", e -> {
        JFrame frame = new ChartOnlyForm(currentFunction);
        openedCharts.add(frame);
        frame.setVisible(true);
    });

    public FunctionsExplorerForm() {
        setupWindow();
        for (AbstractChartFunction func : factory.createFunctions())
            functionList.addElement(func);


        saveButton.setEnabled(false);
        openButton.setEnabled(false);

        JPanel panel = new JPanel(new GridBagLayout());
        int gridx, gridy, gridwidth, gridheight, anchor, fill, ipadx, ipady;
        double weightx, weighty;
        Insets insets;

        anchor = GridBagConstraints.CENTER;
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        panel.add(createFunctionList(), new GridBagConstraints(
                gridx = 0, gridy = 0,
                gridwidth = 1, gridheight = 2,
                weightx = 0, weighty = 1,
                anchor = GridBagConstraints.WEST,
                fill = GridBagConstraints.BOTH,
                insets = new Insets(0, 0, 0, 10),
                ipadx = 0, ipady = 0
        ));
        panel.add(chart, new GridBagConstraints(
                gridx = 1, gridy = 0,
                gridwidth = 3, gridheight = 1,
                weightx = 1, weighty = 0.5,
                anchor = GridBagConstraints.NORTH,
                fill = GridBagConstraints.BOTH,
                insets = new Insets(0, 0, 10, 0),
                ipadx = 0, ipady = 0
        ));
        panel.add(parametersPanel, new GridBagConstraints(
                gridx = 1, gridy = 1,
                gridwidth = 3, gridheight = 1,
                weightx = 1, weighty = 0.5,
                anchor = GridBagConstraints.NORTH,
                fill = GridBagConstraints.BOTH,
                insets = new Insets(0, 0, 10, 0),
                ipadx = 0, ipady = 1
        ));
        panel.add(saveButton, new GridBagConstraints(
                gridx = 1, gridy = 2,
                gridwidth = 1, gridheight = 1,
                weightx = 0, weighty = 0,
                anchor = GridBagConstraints.SOUTHEAST,
                fill = GridBagConstraints.BOTH,
                insets = new Insets(0, 0, 0, 10),
                ipadx = 0, ipady = 0
        ));
        panel.add(openButton, new GridBagConstraints(
                gridx = 2, gridy = 2,
                gridwidth = 1, gridheight = 1,
                weightx = 0, weighty = 0,
                anchor = GridBagConstraints.SOUTHEAST,
                fill = GridBagConstraints.BOTH,
                insets = new Insets(0, 0, 0, 0),
                ipadx = 0, ipady = 0
        ));

        add(panel);
    }

    protected void setupWindow() {
        setTitle("OOP5.Charts");
        setSize(750, 500);
        setResizable(false);
        setCenterLocation();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    void redraw() {
        if (this.currentFunction != null) {
            AbstractChartDrawer drawer = factory.createDrawer(currentFunction);
            chart.setIcon(new ImageIcon(drawer.drawChart(570, 250)));
            //invalidate();
        }
    }

    JSpinner createSpinner(Parameter param) {
        return new JSpinner(new SpinnerNumberModel(
                param.getValue(),
                param.getMinValue(),
                param.getMaxValue(),
                param.getStep()));
    }

    JList<AbstractChartFunction> createFunctionList() {
        JList<AbstractChartFunction> result = new JList<AbstractChartFunction>(functionList);
        result.setCursor(new Cursor(Cursor.HAND_CURSOR));
        result.setLocation(10, 10);
        Dimension size = new Dimension(150, 400);
        result.setSize(size);
        result.setPreferredSize(size);
        result.setMinimumSize(size);
        result.addListSelectionListener(e -> {
            this.currentFunction = result.getSelectedValue();
            openButton.setEnabled(true);
            saveButton.setEnabled(true);
            parametersPanel.removeAll();
            parametersPanel.revalidate();
            parametersPanel.repaint();
            for (Parameter param : this.currentFunction.getParameters()) {
                JSpinner spinner = createSpinner(param);
                spinner.addChangeListener(x -> {
                    param.setValue((double) spinner.getValue());
                    redraw();
                    for (int i = 0; i < openedCharts.size(); i++) {
                        if (!openedCharts.get(i).isVisible())
                            openedCharts.remove(i);
                    }
                    openedCharts.forEach(form -> {
                        form.invalidate();
                        form.repaint();
                    });
                });
                parametersPanel.add(new JLabel(param.getName()));
                parametersPanel.add(spinner);
            }
            redraw();
        });
        //result.setSelectedIndex(1);
        return result;
    }


}

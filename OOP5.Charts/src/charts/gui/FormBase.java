package charts.gui;

import charts.AppFactory;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * Created by Emil on 26.09.2016.
 */
public abstract class FormBase extends JFrame {
    protected AppFactory factory = AppFactory.instance;

    protected void alert(String message) {
        JOptionPane.showMessageDialog(this,
                message,
                "alert",
                JOptionPane.INFORMATION_MESSAGE);
    }

    public void setRandomLocation() {
        Random random = new Random();
        int randInt = random.nextInt(800 - 400) + 400;
        this.setLocation(randInt, randInt / 4);
    }

    public void setCenterLocation() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((screenSize.getWidth() - this.getWidth()) / 2);
        int y = (int) ((screenSize.getHeight() - this.getHeight()) / 2);
        this.setLocation(x, y);
    }
}

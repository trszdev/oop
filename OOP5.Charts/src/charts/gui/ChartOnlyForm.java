package charts.gui;

import charts.AbstractChartDrawer;
import charts.AbstractChartFunction;

import java.awt.*;

/**
 * Created by Emil on 20.09.2016.
 */
public class ChartOnlyForm extends FormBase {
    protected final AbstractChartFunction func;

    public ChartOnlyForm(AbstractChartFunction func)
            throws HeadlessException {
        setTitle(func.getName());
        setSize(800, 600);
        //setResizable(false);
        setRandomLocation();
        this.func = func;
        invalidate();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        AbstractChartDrawer drawer = factory.createDrawer(func);
        Image img = drawer.drawChart(this.getWidth(), this.getHeight() - 30);
        g.drawImage(img, 0, 30, this);
    }
}

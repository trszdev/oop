package charts.gui;

import charts.AbstractChartFunction;
import charts.UserFunction;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Created by Emil on 20.09.2016.
 */
public class SaveChartForm extends FormBase {
    public SaveChartForm(AbstractChartFunction func, DefaultListModel<AbstractChartFunction> lm)
            throws HeadlessException {
        setupWindow();
        JPanel panel = new JPanel(new GridLayout(0, 1, 3, 3));
        JPanel buttonsPanel = new JPanel(new GridLayout(0, 2, 10, 10));
        JTextField newNameField = new JTextField(func.getName());
        JButton saveButton = factory.createButton("Save", e -> {
            lm.addElement(new UserFunction(func, newNameField.getText()));
            this.dispose();
        });
        newNameField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    saveButton.doClick();
            }
        });
        JButton cancelButton = factory.createButton("Cancel", e -> this.dispose());
        buttonsPanel.add(saveButton);
        buttonsPanel.add(cancelButton);
        panel.setBorder(new EmptyBorder(10, 10, 40, 10));
        panel.add(new JLabel("Type new function name:"));
        panel.add(newNameField);
        panel.add(buttonsPanel);
        add(panel);
    }

    void setupWindow() {
        setTitle("Save chart into the list");
        setSize(400, 165);
        setResizable(false);
        setRandomLocation();
    }
}

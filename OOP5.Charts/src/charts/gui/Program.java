package charts.gui;

import javax.swing.*;

/**
 * Created by Emil on 20.09.2016.
 */
public class Program {
    private static void createAndShowGUI() {
        FunctionsExplorerForm form = new FunctionsExplorerForm();
        form.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> createAndShowGUI());
    }
}

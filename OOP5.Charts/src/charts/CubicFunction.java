package charts;

/**
 * Created by Emil on 22.09.2016.
 */
public class CubicFunction extends ChartFunctionBase {
    protected Parameter a;
    protected Parameter b;
    protected Parameter c;
    protected Parameter d;

    public CubicFunction(Parameter a, Parameter b, Parameter c, Parameter d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    @Override
    public String getFormula() {
        return String.format("%sx^3 %sx^2 %sx %s",
                formatSignedDouble(a.getValue()),
                formatDouble(b.getValue()),
                formatDouble(c.getValue()),
                formatDouble(d.getValue()));
    }

    @Override
    public String getCommonFormula() {
        return "Ax^3 + Bx^2 + Cx + D";
    }

    @Override
    public String getName() {
        return "Cubic";
    }

    @Override
    public double getFunctionResult(double x) {
        return x * x * x * a.getValue() + x * x * b.getValue() + x * c.getValue() + d.getValue();
    }

    @Override
    public Parameter[] getParameters() {
        return new Parameter[]{a, b, c, d};
    }

    @Override
    public AbstractChartFunction clone() {
        return new CubicFunction(a.clone(), b.clone(), c.clone(), d.clone());
    }
}

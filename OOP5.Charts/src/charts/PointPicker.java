package charts;

/**
 * Created by Emil on 22.09.2016.
 */
public class PointPicker implements AbstractPointPicker {
    protected final AbstractChartFunction func;
    protected double minPoint;
    protected double maxPoint;
    protected double step;

    public PointPicker(AbstractChartFunction func, double minPoint, double maxPoint, double step) {
        this.func = func;
        this.minPoint = minPoint;
        this.maxPoint = maxPoint;
        this.step = step;
    }

    public PointPicker(AbstractChartFunction func) {
        this(func, -20, 20, 0.1);
    }

    @Override
    public double[] getPoints() {
        int count = (int) ((maxPoint - minPoint) / step);
        double[] result = new double[count--];
        for (double i = maxPoint; count >= 0; i -= step)
            result[count--] = i;
        return result;
    }
}

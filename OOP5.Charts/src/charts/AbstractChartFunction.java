package charts;

/**
 * Created by Emil on 20.09.2016.
 */
// y = f(x)
public interface AbstractChartFunction {
    // returns current formula with subs params
    String getFormula();

    String getCommonFormula();

    String getName();

    double getFunctionResult(double x);

    Parameter[] getParameters();

    AbstractChartFunction clone();
}

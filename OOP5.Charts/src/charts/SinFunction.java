package charts;

/**
 * Created by Emil on 28.09.2016.
 */
public class SinFunction extends ChartFunctionBase {
    @Override
    public String getFormula() {
        return "sinx";
    }

    @Override
    public String getCommonFormula() {
        return "sinx";
    }

    @Override
    public String getName() {
        return "Sine";
    }

    @Override
    public double getFunctionResult(double x) {
        return Math.sin(x);
    }

    @Override
    public Parameter[] getParameters() {
        return new Parameter[0];
    }

    @Override
    public AbstractChartFunction clone() {
        return new SinFunction();
    }
}

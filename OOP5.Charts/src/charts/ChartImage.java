package charts;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by Emil on 04.10.2016.
 */

// TODO: bad usage
public class ChartImage extends BufferedImage {
    public final int hheight;
    public final int height;
    public final int width;
    public final int hwidth;

    public ChartImage(int width, int height) {
        super(width, height, BufferedImage.TYPE_INT_RGB);
        this.width = width;
        this.height = height;
        this.hwidth = width / 2;
        this.hheight = height / 2;
    }

    @Override
    public Graphics2D createGraphics() {
        Graphics2D g = super.createGraphics();
        g.setColor(Color.white);
        g.fillRect(0, 0, width, height);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(Color.gray);
        g.drawLine(hwidth, 0, hwidth, height);
        g.drawLine(0, hheight, width, hheight);
        return g;
    }
}

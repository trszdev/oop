package charts;

import java.awt.*;

/**
 * Created by Emil on 22.09.2016.
 */
public class ChartDrawer implements AbstractChartDrawer {
    protected AbstractChartFunction func;
    protected AbstractPointPicker pointPicker;

    public ChartDrawer(AbstractChartFunction func, AbstractPointPicker pointPicker) {
        this.func = func;
        this.pointPicker = pointPicker;
    }


    void drawFunctionName(Graphics2D g, int width, int height) {
        int size = Math.min(width, height);
        final Font titleFont = new Font("Arial", Font.BOLD, size / 10);
        final Font formulaFont = new Font("Arial", Font.PLAIN, size / 11);
        g.setFont(titleFont);
        g.setColor(Color.black);
        g.drawString(func.getName(), size / 10, size / 10);
        g.setFont(formulaFont);
        g.setColor(Color.darkGray);
        g.drawString(func.getFormula(), size / 10, size / 10 + size / 11);
    }


    // TODO
    @Override
    public Image drawChart(int width, int height) {
        ChartImage result = new ChartImage(width, height);
        Graphics2D g = result.createGraphics();

        double[] dots = pointPicker.getPoints();
        int pointMod = dots.length / 2;
        if (dots == null || dots.length == 0) {
            drawFunctionName(g, width, height);
            return result;
        }
        double leftDot = dots[0], rightDot = dots[0],
                highDot = func.getFunctionResult(dots[0]),
                lowDot = func.getFunctionResult(dots[0]);

        for (double dot : dots) {
            double dotValue = func.getFunctionResult(dot);
            if (dot > rightDot) rightDot = dot;
            if (dot < leftDot) leftDot = dot;
            if (dotValue > highDot) highDot = dotValue;
            if (dotValue < lowDot) lowDot = dotValue;
        }

        int hwidth = result.hwidth;
        int hheight = result.hheight;
        int scale = (int) Math.min(Math.max(height / -lowDot, height / highDot), Math.max(width / -leftDot, width / rightDot)) + 1;

        g.drawLine(hwidth - 4, hheight - scale, hwidth + 4, hheight - scale);
        g.setColor(Color.BLUE);
        g.setStroke(new BasicStroke(3));
        g.setFont(new Font("Arial", Font.PLAIN, 14));
        double lastDot = Double.NaN;
        double lastDotValue = Double.NaN;
        int i = 0;
        for (double dot : pointPicker.getPoints()) {
            double dotValue = func.getFunctionResult(dot);
            i++;
            if (Double.isNaN(lastDot) || Double.isInfinite(lastDot) ||
                    Double.isNaN(lastDotValue) || Double.isInfinite(lastDotValue)) {
                lastDot = dot;
                lastDotValue = dotValue;
                continue;
            }
            if (Double.isNaN(dotValue) || Double.isInfinite(dotValue)) {
                lastDot = Double.NaN;
                continue;
            }
            int x = (int) (dot * scale) + hwidth;
            int y = (int) (-dotValue * scale) + hheight;
            g.drawLine(x, y,
                    (int) (lastDot * scale) + hwidth, (int) (-lastDotValue * scale) + hheight);
            if (i++ % pointMod == 0) {
                g.setColor(Color.red);
                g.drawOval(x - 1, y - 1, 3, 3);
                g.drawString(String.format("{x=%.1f  y=%.1f}", dot, dotValue), x, y);
                g.setColor(Color.BLUE);
            }
            lastDot = dot;
            lastDotValue = dotValue;
        }
        drawFunctionName(g, width, height);
        return result;
    }
}

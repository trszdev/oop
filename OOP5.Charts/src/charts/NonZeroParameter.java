package charts;

/**
 * Created by Emil on 22.09.2016.
 */
public class NonZeroParameter extends Parameter {
    public NonZeroParameter(String name, double step, double value, double maxValue, double minValue) {
        super(step, value, maxValue, minValue, name);
        if (value == 0)
            throw new IllegalArgumentException("Value must not be zero");
    }
}

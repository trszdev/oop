package charts;

/**
 * Created by Emil on 22.09.2016.
 */
public interface AbstractPointPicker {
    double[] getPoints();
}

package charts;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by Emil on 22.09.2016.
 */
public abstract class ChartFunctionBase implements AbstractChartFunction {
    static NumberFormat nf = new DecimalFormat("#.###");

    protected String formatSignedDouble(double value) {
        return nf.format(value);
    }

    protected String formatDouble(double value) {
        StringBuilder sb = new StringBuilder();
        sb.append(value >= 0 ? "+ " : "- ");
        sb.append(nf.format(Math.abs(value)));
        return sb.toString();
    }

    @Override
    public String toString() {
        return this.getName();
    }

    public abstract String getFormula();

    public abstract String getCommonFormula();

    public abstract String getName();

    public abstract double getFunctionResult(double x);

    public abstract Parameter[] getParameters();

    public abstract AbstractChartFunction clone();
}

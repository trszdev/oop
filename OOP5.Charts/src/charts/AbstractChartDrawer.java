package charts;

import java.awt.*;

/**
 * Created by Emil on 22.09.2016.
 */
public interface AbstractChartDrawer {
    Image drawChart(int width, int height);
}

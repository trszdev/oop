package charts;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by Emil on 23.09.2016.
 */
public class AppFactory {
    public final static AppFactory instance = new AppFactory();

    private AppFactory() {
    }

    public Parameter createParameter(String name, double value) {
        return new Parameter(0.5, value, 40, -40, name);
    }

    public AbstractPointPicker createPointPicker(AbstractChartFunction f) {
        return new PointPicker(f);
    }

    public AbstractChartDrawer createDrawer(AbstractChartFunction f) {
        return new ChartDrawer(f, createPointPicker(f));
    }

    public JButton createButton(String title, ActionListener l) {
        JButton result = new JButton(title);
        result.setCursor(new Cursor(Cursor.HAND_CURSOR));
        result.addActionListener(l);
        return result;
    }

    public AbstractChartFunction[] createFunctions() {
        return new AbstractChartFunction[]{
                new SinFunction(),
                new QuadraticFunction(createParameter("A", 1), createParameter("B", 2), createParameter("C", 4)),
                new LinearFunction(createParameter("A", 2), createParameter("B", 3)),
                new HyperbolaFunction(createParameter("A", 1), createParameter("B", 0)),
                new CubicFunction(createParameter("A", 1), createParameter("B", 2), createParameter("C", 4), createParameter("D", 4)),
        };
    }
}

package charts;

/**
 * Created by Emil on 22.09.2016.
 */
public class QuadraticFunction extends ChartFunctionBase {
    protected Parameter a;
    protected Parameter b;
    protected Parameter c;


    public QuadraticFunction(Parameter a, Parameter b, Parameter c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public String getFormula() {
        return String.format("%sx^2 %sx %s", formatSignedDouble(a.getValue()), formatDouble(b.getValue()), formatDouble(c.getValue()));
    }

    @Override
    public String getCommonFormula() {
        return "Ax^2 + Bx + C";
    }

    @Override
    public String getName() {
        return "Quadratic";
    }

    @Override
    public double getFunctionResult(double x) {
        return x * x * a.getValue() + x * b.getValue() + c.getValue();
    }

    @Override
    public Parameter[] getParameters() {
        return new Parameter[]{a, b, c};
    }

    @Override
    public AbstractChartFunction clone() {
        return new QuadraticFunction(a.clone(), b.clone(), c.clone());
    }
}

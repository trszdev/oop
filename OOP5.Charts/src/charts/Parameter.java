package charts;

/**
 * Created by Emil on 20.09.2016.
 */
public class Parameter {
    double step;
    double value;
    double maxValue;
    double minValue;
    String name;

    public Parameter(double step, double value, double maxValue, double minValue, String name) {
        this.step = step;
        this.maxValue = maxValue;
        this.minValue = minValue;
        this.name = name;
        setValue(value);
    }

    public String getName() {
        return name;
    }

    public double getMaxValue() {
        return maxValue;
    }

    public double getMinValue() {
        return minValue;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getStep() {
        return step;
    }

    protected Parameter clone() {
        return new Parameter(step, value, maxValue, minValue, name);
    }
}

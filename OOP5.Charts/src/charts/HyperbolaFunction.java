package charts;

/**
 * Created by Emil on 22.09.2016.
 */
public class HyperbolaFunction extends ChartFunctionBase {
    protected Parameter a;
    protected Parameter b;

    public HyperbolaFunction(Parameter a, Parameter b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public String getFormula() {
        return String.format("%s/x %s", formatSignedDouble(a.getValue()), formatDouble(b.getValue()));
    }

    @Override
    public String getCommonFormula() {
        return "A/x + B";
    }

    @Override
    public String getName() {
        return "Hyperbola";
    }

    @Override
    public double getFunctionResult(double x) {
        if (Math.abs(x) < 0.0000001) return Double.NaN;
        return a.getValue() / x + b.getValue();
    }

    @Override
    public Parameter[] getParameters() {
        return new Parameter[]{a, b};
    }

    @Override
    public AbstractChartFunction clone() {
        return new HyperbolaFunction(a.clone(), b.clone());
    }
}

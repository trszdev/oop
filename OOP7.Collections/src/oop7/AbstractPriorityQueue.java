package oop7;

/**
 * Created by Emil on 17.10.2016.
 */
public interface AbstractPriorityQueue<TKey extends Comparable, TValue>
        extends Iterable<Tuple<TKey, TValue>> {
    void push(TKey key, TValue value); // - добавляет элемент value с ключом key

    Tuple<TKey, TValue> pop(); // - считывает элемент с наименьшим приоритетом и удаляет из очереди

    Tuple<TKey, TValue> shift(); // - считывает элемент с наивысшим приоритетом и удаляет из очереди

    Tuple<TKey, TValue> first(); // - считывает элемент с наивысшим приоритетом

    Tuple<TKey, TValue> last(); // - считывает элемент с наименьшим приоритетом

    int size(); //- общее количество элементов
}

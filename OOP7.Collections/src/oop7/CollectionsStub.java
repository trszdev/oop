package oop7;

import java.util.Map;

/**
 * Created by Emil on 29.10.2016.
 */
public final class CollectionsStub implements AbstractCollectionsFactory {
    private final Map map;
    private final AbstractPriorityQueue queue;
    private final AbstractGraph graph;
    private final AbstractBinarySearchTree tree;

    public CollectionsStub(Map map, AbstractPriorityQueue queue, AbstractGraph graph, AbstractBinarySearchTree tree) {
        this.map = map;
        this.queue = queue;
        this.graph = graph;
        this.tree = tree;
    }

    @Override
    public <K, V> Map<K, V> createMap() {
        return this.map;
    }

    @Override
    public <K extends Comparable, V> AbstractPriorityQueue<K, V> createPriorityQueue() {
        return this.queue;
    }

    @Override
    public <T> AbstractGraph<T> createGraph() {
        return this.graph;
    }

    @Override
    public <K extends Comparable, V> AbstractBinarySearchTree<K, V> createBst() {
        return this.tree;
    }
}

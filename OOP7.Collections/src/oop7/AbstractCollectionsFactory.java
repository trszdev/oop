package oop7;

import java.util.Map;

/**
 * Created by Emil on 29.10.2016.
 */
public interface AbstractCollectionsFactory {
    <K, V> Map<K, V> createMap();

    <K extends Comparable, V> AbstractPriorityQueue<K, V> createPriorityQueue();

    <T> AbstractGraph<T> createGraph();

    <K extends Comparable, V> AbstractBinarySearchTree<K, V> createBst();
}

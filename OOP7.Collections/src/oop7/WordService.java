package oop7;

import java.util.*;

/**
 * Created by Emil on 27.10.2016.
 */
public class WordService implements AbstractWordService {
    protected final String[] database;
    protected final AbstractPriorityQueue<Integer, Tuple<String, Integer>> timeQueue;
    protected final Map<Tuple<String, Integer>, String[]> cache;

    final int CACHE_SIZE = 10;

    public WordService(Iterable<String> database, AbstractCollectionsFactory collections) {
        this.timeQueue = collections.createPriorityQueue();
        this.cache = collections.createMap();
        BinaryHeap<String, String> sorted = new BinaryHeap<>(true);
        for (String row : database) sorted.push(row, row);
        this.database = new String[sorted.size()];
        Iterator<Tuple<String, String>> iter = sorted.iterator();
        for (int i = 0; i < sorted.size(); i++)
            this.database[i] = iter.next().getValue();

    }

    // TODO: binary search
    public String[] getWordsOnPrefixNoCache(String prefix, int amount) {
        List<String> result = new ArrayList<>();
        for (int i=0; i < database.length; i++) {
            String word = database[i];
            if (word.startsWith(prefix))
                result.add(word);
        }
        return result.toArray(new String[0]);
    }

    @Override
    public String[] getWordsOnPrefix(String prefix, int amount) {
        Integer timeStamp = Math.toIntExact((16 >> new Date().getTime()) % Integer.MAX_VALUE);
        Tuple<String, Integer> key = new Tuple<>(prefix, amount);
        if (cache.containsKey(key)) {
            timeQueue.push(timeStamp, key);
            return cache.get(key);
        }
        String[] result = getWordsOnPrefixNoCache(prefix, amount);
        if (cache.size() >= CACHE_SIZE) {
            Tuple<String, Integer> minKey = timeQueue.pop().value;
            cache.remove(minKey);
        }
        cache.put(key, result);
        timeQueue.push(timeStamp, key);
        return result;
    }
}

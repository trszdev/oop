package oop7;

/**
 * Created by Emil on 17.10.2016.
 */
public class Edge<T> {
    protected final boolean isOriented;
    protected final T parent;
    protected final T child;

    public Edge(T parent, T child) {
        this(parent, child, false);
    }

    public Edge(T parent, T child, boolean isOriented) {
        this.isOriented = isOriented;
        this.parent = parent;
        this.child = child;
    }

    public boolean isOriented() {
        return isOriented;
    }

    public T getParent() {
        return parent;
    }

    public T getChild() {
        return child;
    }

    @Override
    public int hashCode() {
        return parent.hashCode() ^ child.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        Edge edge = (Edge) obj;
        if (isOriented &&
                (!parent.equals(edge.getParent()) || !child.equals(edge.getChild())))
            return false;
        return (parent.equals(edge.getParent()) && child.equals(edge.getChild())) ||
                (parent.equals(edge.getChild()) && child.equals(edge.getParent()));
    }

    @Override
    public String toString() {
        return "Edge{" +
                "isOriented=" + isOriented +
                ", parent=" + parent +
                ", child=" + child +
                '}';
    }
}

package oop7;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;

/**
 * Created by Emil on 30.10.2016.
 */
public class FriendService implements AbstractFriendService {
    protected final AbstractGraph<String> graph;

    public FriendService(Iterable<String> database, AbstractCollectionsFactory collections) {
        this.graph = collections.createGraph();
        for (String row : database) {
            String[] friends = row.split("-");
            graph.addEdge(friends[0].trim(), friends[1].trim());
        }

    }

    @Override
    public String[] getPossibleFriends(String name, int amount) {
        Set<String> result = new HashSet<>();
        Set<String> closeFriends = new HashSet<>();
        Queue<String> queue = new ArrayDeque<>();
        for (String closeFriend : graph.getAdjacent(name)) {
            queue.add(closeFriend);
            closeFriends.add(closeFriend);
        }
        outer: while (!queue.isEmpty()) {
            String someone = queue.poll();
            for (String possibleFriend : graph.getAdjacent(someone)) {
                if (name.equals(possibleFriend) || result.contains(possibleFriend) ||
                        closeFriends.contains(possibleFriend))
                    continue;
                if (result.size() >= amount) break outer;
                result.add(possibleFriend);
                queue.add(possibleFriend);
            }
        }
        return result.toArray(new String[0]);
    }
}

package oop7;

/**
 * Created by Emil on 17.10.2016.
 */
public interface AbstractGraph<T> extends Iterable<Edge<T>> {
    void add(T node); // - добавляет изолированную вершину u

    void addEdge(T a, T b); //- добавляет ребро, соединяющее u и v. Если таких вершин в графе нет, добавляет их

    void deleteEdge(T a, T b); //  - удаляет ребро, оставляя вершины. Если такого ребра нет, ничего не происходит

    boolean isEmpty();// - проверка на пустоту

    T[] getAdjacent(T a);// - возвращает список всех вершин, смежных с v
}

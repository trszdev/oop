package oop7;

/**
 * Created by Emil on 17.10.2016.
 */
public interface AbstractBinarySearchTree<TKey extends Comparable, TValue>
        extends AbstractPriorityQueue<TKey, TValue> {
    TValue read(TKey key); // - чтение элемента по ключу

    void delete(TKey key); // - удаление элемента по ключу

    Tuple<TKey, TValue> top(); // - возращает ссылку элемент на вершине дерева
}

package oop7;

/**
 * Created by Emil on 30.10.2016.
 */
public interface AbstractFriendService {
    String[] getPossibleFriends(String name, int amount);
}

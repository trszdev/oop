package oop7;

/**
 * Created by Emil on 17.10.2016.
 */
public class Tuple<TKey, TValue> implements Cloneable {
    protected final TKey key;
    protected final TValue value;

    public Tuple(TKey key, TValue value) {
        this.key = key;
        this.value = value;
    }

    public TKey getKey() {
        return key;
    }

    public TValue getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        return key.hashCode() ^ value.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        Tuple tuple = (Tuple) obj;
        return key.equals(tuple.getKey()) &&
                value.equals(tuple.getValue());
    }

    @Override
    public String toString() {
        return "Tuple{" +
                "key=" + key +
                ", value=" + value +
                '}';
    }
}

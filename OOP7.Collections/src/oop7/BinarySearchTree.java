package oop7;

import java.util.Collections;
import java.util.Iterator;
import java.util.Stack;

class BstNode<TKey, TValue> {
    public BstNode<TKey, TValue> parent;
    public BstNode<TKey, TValue> lower;
    public BstNode<TKey, TValue> greater;
    public Tuple<TKey, TValue> kv;
    public boolean isGreaterChild;
}

/**
 * Created by Emil on 17.10.2016.
 */
// TODO: AA tree or just balancing + iterate
public class BinarySearchTree<TKey extends Comparable, TValue>
        implements AbstractBinarySearchTree<TKey, TValue> {

    private BstNode<TKey, TValue> root = null;
    private BstNode<TKey, TValue> first = null;
    private BstNode<TKey, TValue> last = null;
    private int size;

    private void recalcVertices() {
        if (root == null) {
            first = null;
            last = null;
            return;
        }
        first = findMax(root);
        last = findMin(root);
    }

    @Override
    public void push(TKey key, TValue value) {
        BstNode<TKey, TValue> newNode = new BstNode<>();
        newNode.kv = new Tuple<>(key, value);

        if (root != null) {
            BstNode<TKey, TValue> nearest = getNearestNode(key);
            int comparison = key.compareTo(nearest.kv.key);
            if (comparison == 0) {
                size--;
                nearest.kv = newNode.kv;
            }
            if (comparison < 0) nearest.lower = newNode;
            if (comparison > 0) {
                nearest.greater = newNode;
                newNode.isGreaterChild = true;
            }
            newNode.parent = nearest;
        } else root = newNode;
        size++;
        recalcVertices();
    }

    @Override
    public Tuple<TKey, TValue> pop() {
        Tuple<TKey, TValue> result = last();
        if (result == null) return null;
        delete(result.key);
        return result;
    }

    @Override
    public Tuple<TKey, TValue> shift() {
        Tuple<TKey, TValue> result = first();
        if (result == null) return null;
        delete(result.key);
        return result;
    }

    // doesn't check root!
    // returns node if exists else parent
    private BstNode<TKey, TValue> getNearestNode(TKey key) {
        BstNode<TKey, TValue> current = root;
        while (true) {
            int comparison = key.compareTo(current.kv.key);
            if (comparison == 0) return current;
            if (comparison < 0 && current.lower == null)
                return current;
            if (comparison > 0 && current.greater == null)
                return current;
            current = comparison > 0 ? current.greater : current.lower;
        }
    }

    private BstNode<TKey, TValue> findMin(BstNode<TKey, TValue> start) {
        while (start.lower != null)
            start = start.lower;
        return start;
    }

    private BstNode<TKey, TValue> findMax(BstNode<TKey, TValue> start) {
        while (start.greater != null)
            start = start.greater;
        return start;
    }

    @Override
    public TValue read(TKey key) {
        if (root == null) return null;
        BstNode<TKey, TValue> nearest = getNearestNode(key);
        if (!nearest.kv.key.equals(key)) return null;
        return nearest.kv.value;
    }

    @Override
    public void delete(TKey key) {
        if (root == null) return;
        BstNode<TKey, TValue> nodeToDelete = getNearestNode(key);
        if (!nodeToDelete.kv.key.equals(key)) return; // no such node
        BstNode<TKey, TValue> newNode = null;
        boolean hasGreater = nodeToDelete.greater != null;
        boolean hasLower = nodeToDelete.lower != null;

        if (hasGreater && hasLower) {
            newNode = findMin(nodeToDelete.greater);
            if (newNode.isGreaterChild) newNode.parent.greater = null;
            else newNode.parent.lower = null;
            newNode.lower = nodeToDelete.lower;
            newNode.greater = nodeToDelete.greater;
        } else if (!hasGreater && !hasLower) newNode = null;
        else if (hasGreater && !hasLower) newNode = nodeToDelete.greater;
        else if (!hasGreater && hasLower) newNode = nodeToDelete.lower;


        if (nodeToDelete != root) {
            if (nodeToDelete.isGreaterChild) nodeToDelete.parent.greater = newNode;
            else nodeToDelete.parent.lower = newNode;
        } else root = newNode;
        size--;
        recalcVertices();
    }

    @Override
    public Tuple<TKey, TValue> top() {
        if (root == null) return null;
        return root.kv;
    }

    @Override
    public Tuple<TKey, TValue> first() {
        if (first == null) return null;
        return first.kv;
    }

    @Override
    public Tuple<TKey, TValue> last() {
        if (last == null) return null;
        return last.kv;
    }

    @Override
    public int size() {
        return this.size;
    }

    // not a lazy iterator...
    @Override
    public Iterator<Tuple<TKey, TValue>> iterator() {
        if (root == null) return Collections.emptyIterator();
        BstNode<TKey, TValue> current = root;
        Stack<BstNode<TKey, TValue>> stack = new Stack<>();
        Stack<Tuple<TKey, TValue>> result = new Stack<>();
        while (true) {
            if (current != null) {
                stack.push(current);
                current = current.lower;
            } else {
                if (!stack.isEmpty()) {
                    current = stack.pop();
                    result.push(current.kv);
                    current = current.greater;
                } else break;
            }
        }
        return result.iterator();
    }
}

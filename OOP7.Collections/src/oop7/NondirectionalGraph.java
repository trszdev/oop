package oop7;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by Emil on 17.10.2016.
 */
public class NondirectionalGraph<T> implements AbstractGraph<T> {
    protected final Map<T, Set<T>> edgeMap = new HashMap<>();
    protected final Set<Edge<T>> edges = new HashSet<>();

    @Override
    public void add(T node) {
        if (!edgeMap.containsKey(node))
            edgeMap.put(node, new HashSet<>());
    }

    @Override
    public void addEdge(T a, T b) {
        add(a);
        add(b);
        edgeMap.get(a).add(b);
        edgeMap.get(b).add(a);
        edges.add(new Edge<>(a, b));
    }

    @Override
    public void deleteEdge(T a, T b) {
        add(a);
        add(b);
        edgeMap.get(a).remove(b);
        edgeMap.get(b).remove(a);
        edges.remove(new Edge<>(a, b));
    }

    @Override
    public boolean isEmpty() {
        return edgeMap.isEmpty();
    }

    @Override
    public T[] getAdjacent(T a) {
        Set<T> adjacent = edgeMap.get(a);
        if (adjacent == null) return (T[]) Array.newInstance(a.getClass(), 0);
        return adjacent.toArray((T[]) Array.newInstance(a.getClass(), 0));
    }

    @Override
    public Iterator<Edge<T>> iterator() {
        return edges.iterator();
    }
}

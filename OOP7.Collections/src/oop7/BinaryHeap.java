package oop7;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by Emil on 23.10.2016.
 */
public class BinaryHeap<TPriority extends Comparable, TValue>
        implements Iterable<Tuple<TPriority, TValue>> {
    protected final boolean shouldPeekMinimum;
    private int size = 0;
    private int capacity = 0;
    private Tuple<TPriority, TValue>[] heap;


    public BinaryHeap(boolean shouldPeekMinimum) {
        this(shouldPeekMinimum, 10);
    }

    public BinaryHeap(boolean shouldPeekMinimum, int capacity) {
        this.shouldPeekMinimum = shouldPeekMinimum;
        this.capacity = capacity;
        heap = new Tuple[capacity];
    }

    private void reallocHeap() {
        this.capacity *= 2;
        heap = Arrays.copyOf(heap, this.capacity);
    }

    private void heapify(Tuple<TPriority, TValue>[] heap, int size, int index) {
        int sign = shouldPeekMinimum ? -1 : 1;
        for (; ; ) {
            int leftChild = 2 * index + 1;
            int rightChild = 2 * index + 2;
            int largestChild = index;

            if (leftChild < size && heap[leftChild].key.compareTo(heap[largestChild].key) * sign > 0)
                largestChild = leftChild;

            if (rightChild < size && heap[rightChild].key.compareTo(heap[largestChild].key) * sign > 0)
                largestChild = rightChild;

            if (largestChild == index)
                break;

            Tuple<TPriority, TValue> temp = heap[index];
            heap[index] = heap[largestChild];
            heap[largestChild] = temp;
            index = largestChild;
        }
    }


    public void push(TPriority priority, TValue value) {
        int i = size++;
        if (size == capacity) reallocHeap();
        heap[i] = new Tuple<>(priority, value);


        int parentIndex = (i - 1) / 2;
        final int sign = this.shouldPeekMinimum ? -1 : 1;
        while (i > 0 && heap[i].key.compareTo(heap[parentIndex].key) * sign > 0) {
            Tuple<TPriority, TValue> temp = heap[i];
            heap[i] = heap[parentIndex];
            heap[parentIndex] = temp;
            i = parentIndex;
            parentIndex = (i - 1) / 2;
        }
    }

    public Tuple<TPriority, TValue> pop() {
        if (size == 0) return null;
        size--;
        Tuple<TPriority, TValue> result = heap[0];
        heap[0] = heap[size];
        heap[size] = null;
        heapify(this.heap, this.size, 0);
        return result;
    }


    public Tuple<TPriority, TValue> peek() {
        if (size == 0) return null;
        return heap[0];
    }


    public int size() {
        return size;
    }


    public Iterator<Tuple<TPriority, TValue>> iterator() {
        Tuple<TPriority, TValue>[] heapCopy = Arrays.copyOf(heap, size);
        return new Iterator<Tuple<TPriority, TValue>>() {
            private int count = size;

            @Override
            public boolean hasNext() {
                return count > 0;
            }

            @Override
            public Tuple<TPriority, TValue> next() {
                if (count == 0) return null;
                count--;
                Tuple<TPriority, TValue> result = heapCopy[0];
                heapCopy[0] = heapCopy[count];
                heapCopy[count] = null;
                heapify(heapCopy, count, 0);
                return result;
            }
        };
    }
}

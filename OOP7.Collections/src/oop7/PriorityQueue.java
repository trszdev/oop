package oop7;

import java.util.Iterator;

/**
 * Created by Emil on 29.10.2016.
 */
public class PriorityQueue<TKey extends Comparable, TValue>
        implements AbstractPriorityQueue<TKey, TValue> {
    protected final BinaryHeap<TKey, TValue> minHeap;
    protected final BinaryHeap<TKey, TValue> maxHeap;

    public PriorityQueue(BinaryHeap<TKey, TValue> minHeap, BinaryHeap<TKey, TValue> maxHeap) {
        this.minHeap = minHeap;
        this.maxHeap = maxHeap;
    }

    public PriorityQueue(int capacity) {
        this.minHeap = new BinaryHeap<>(true, capacity);
        this.maxHeap = new BinaryHeap<>(false, capacity);
    }

    public PriorityQueue() {
        this(10);
    }


    @Override
    public void push(TKey key, TValue value) {
        minHeap.push(key, value);
        maxHeap.push(key, value);
    }

    @Override
    public Tuple<TKey, TValue> pop() {
        return minHeap.pop();
    }

    @Override
    public Tuple<TKey, TValue> shift() {
        return maxHeap.pop();
    }

    @Override
    public Tuple<TKey, TValue> first() {
        return maxHeap.peek();
    }

    @Override
    public Tuple<TKey, TValue> last() {
        return minHeap.peek();
    }

    @Override
    public int size() {
        return maxHeap.size();
    }

    @Override
    public Iterator<Tuple<TKey, TValue>> iterator() {
        return minHeap.iterator();
    }
}

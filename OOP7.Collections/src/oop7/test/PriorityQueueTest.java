package oop7.test;

import oop7.AbstractPriorityQueue;
import oop7.PriorityQueue;
import oop7.Tuple;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by Emil on 27.10.2016.
 */
public class PriorityQueueTest {
    public AbstractPriorityQueue<Integer, Integer> getPriorityQueue() {
        return new PriorityQueue<>();
    }

    @Test
    public void checkEmpty() throws Exception {
        AbstractPriorityQueue<Integer, Integer> q = getPriorityQueue();
        assertEquals(0, q.size());
        assertEquals(null, q.first());
        assertEquals(null, q.last());
        assertEquals(null, q.shift());
        assertEquals(null, q.pop());
        assertEquals(0, q.size());
    }

    void checkIterate(Integer[] initial, Integer[] sorted) {
        AbstractPriorityQueue<Integer, Integer> q = getPriorityQueue();
        for (int value : initial)
            q.push(value, value);
        List<Integer> s = new ArrayList<>();
        for (Tuple<Integer, Integer> kv : q) s.add(kv.getValue());
        assertArrayEquals(s.toArray(), sorted);
    }

    @Test
    public void checkIterate1() throws Exception {
        checkIterate(new Integer[]{0, 2, 3, 9, 6, 1, 4, 5, 8, 7},
                new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
    }


    @Test
    public void checkIterate2() throws Exception {
        checkIterate(new Integer[]{9, 8, 7, 6, 5, 4, 3, 2, 1, 0},
                new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
    }

    void checkRange(int minBound, int maxBound) {
        AbstractPriorityQueue<Integer, Integer> q = getPriorityQueue();
        for (int i = minBound; i <= maxBound; i++) {
            q.push(i, i);
        }
        assertEquals(maxBound - minBound + 1, q.size());
        for (int i = maxBound; i >= minBound; i--) {
            assertEquals(i - minBound + 1, q.size());
            assertEquals(i, q.first().getKey().intValue());
            assertEquals(i, q.shift().getKey().intValue());
        }
        assertEquals(0, q.size());
    }

    @Test
    public void checkRange100k_positiveOnly() throws Exception {
        checkRange(1, 100_000);
    }

    @Test
    public void checkRange150k_mixed() throws Exception {
        checkRange(-50000, 100_000);
    }

    @Test
    public void checkRange100k_negative() throws Exception {
        checkRange(-500000, -400000);
    }
}
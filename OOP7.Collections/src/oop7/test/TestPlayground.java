package oop7.test;

import oop7.*;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by Emil on 31.10.2016.
 */
public class TestPlayground {
    public static void main(String[] args) {
        FriendService f = new FriendService(Arrays.asList(
                "vova - nikolay", "olga - lena", "sasha - dima", "sveta - vova"
        ), new CollectionsStub(
                new HashMap(), new PriorityQueue(), new NondirectionalGraph(), new BinarySearchTree()
        ));
        System.out.println(Arrays.toString(f.getPossibleFriends("nikolay", 2)));
    }
}

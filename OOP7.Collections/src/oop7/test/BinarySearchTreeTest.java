package oop7.test;

import oop7.AbstractBinarySearchTree;
import oop7.BinarySearchTree;
import oop7.Tuple;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Emil on 24.10.2016.
 */
public class BinarySearchTreeTest {
    static AbstractBinarySearchTree<Integer, String> createTree() {
        return new BinarySearchTree<>();
    }

    @Test
    public void testEmpty1() throws Exception {
        AbstractBinarySearchTree<Integer, String> tree = createTree();
        assertEquals(tree.size(), 0);
        assertEquals(tree.last(), null);
        assertEquals(tree.top(), null);
        assertEquals(tree.first(), null);
        assertEquals(tree.read(0), null); // default key

    }

    @Test
    public void insertOnly_same1() throws Exception {
        AbstractBinarySearchTree<Integer, String> tree = createTree();
        tree.push(1, "example");
        tree.push(1, "example");
        assertEquals(tree.size(), 1);
        assertEquals(tree.last(), new Tuple<>(1, "example"));
        assertEquals(tree.top(), new Tuple<>(1, "example"));
        assertEquals(tree.first(), new Tuple<>(1, "example"));
        assertEquals(tree.read(0), null); // default key
        assertEquals(tree.read(1), "example");
    }

    @Test
    public void insertOnly_same2() throws Exception {
        AbstractBinarySearchTree<Integer, String> tree = createTree();
        tree.push(1, "example");
        tree.push(1, "example1");
        assertEquals(tree.size(), 1);
        assertEquals(tree.last(), new Tuple<>(1, "example1"));
        assertEquals(tree.top(), new Tuple<>(1, "example1"));
        assertEquals(tree.first(), new Tuple<>(1, "example1"));
        assertEquals(tree.read(0), null); // default key
        assertEquals(tree.read(1), "example1");
    }


    @Test
    public void common1() throws Exception {
        AbstractBinarySearchTree<Integer, String> tree = createTree();
        tree.push(2, "test2");
        tree.push(3, "test3");
        tree.push(1, "test1");
        //tree.delete(1);
        assertEquals(tree.read(2), "test2");
        assertEquals(tree.read(3), "test3");
        //assertEquals(tree.read(1), null);
    }

    @Test
    public void common2() throws Exception {
        AbstractBinarySearchTree<Integer, String> tree = createTree();
        tree.push(2, "test1");
        tree.push(3, "test2");
        tree.push(5, "big");
        tree.push(4, "huge");
        tree.push(1, "test3");
        tree.delete(3);
        assertEquals(tree.read(1), "test3");
        assertEquals(tree.read(2), "test1");
        assertEquals(tree.read(3), null);
        assertEquals(tree.read(4), "huge");
        assertEquals(tree.read(5), "big");
        assertEquals(tree.size(), 4);
        assertEquals(tree.first(), new Tuple<>(5, "big"));
        assertEquals(tree.top(), new Tuple<>(2, "test1"));
        assertEquals(tree.last(), new Tuple<>(1, "test3"));
    }

    // common3-5 tests at:
    // http://www.algolist.net/Data_structures/Binary_search_tree/Removal

    @Test
    public void common3() throws Exception {
        AbstractBinarySearchTree<Integer, String> tree = createTree();
        tree.push(5, "5");
        tree.push(18, "18");
        tree.push(2, "2");
        tree.push(-4, "-4");
        tree.push(3, "3");

        tree.delete(-4);
        assertEquals(tree.read(5), "5");
        assertEquals(tree.read(18), "18");
        assertEquals(tree.read(2), "2");
        assertEquals(tree.read(-4), null);
        assertEquals(tree.read(3), "3");
        assertEquals(tree.size(), 4);
        assertEquals(tree.first(), new Tuple<>(18, "18"));
        assertEquals(tree.top(), new Tuple<>(5, "5"));
        assertEquals(tree.last(), new Tuple<>(2, "2"));
    }

    @Test
    public void common4() throws Exception {
        AbstractBinarySearchTree<Integer, String> tree = createTree();
        tree.push(5, "5");
        tree.push(18, "18");
        tree.push(21, "21");
        tree.push(25, "25");
        tree.push(19, "19");
        tree.push(2, "2");
        tree.push(-4, "-4");
        tree.push(3, "3");

        tree.delete(18);
        assertEquals(tree.read(5), "5");
        assertEquals(tree.read(18), null);
        assertEquals(tree.read(2), "2");
        assertEquals(tree.read(-4), "-4");
        assertEquals(tree.read(21), "21");
        assertEquals(tree.read(25), "25");
        assertEquals(tree.read(19), "19");
        assertEquals(tree.read(3), "3");
        assertEquals(tree.size(), 7);
        assertEquals(tree.first(), new Tuple<>(25, "25"));
        assertEquals(tree.top(), new Tuple<>(5, "5"));
        assertEquals(tree.last(), new Tuple<>(-4, "-4"));
    }

    @Test
    public void common5() throws Exception {
        AbstractBinarySearchTree<Integer, String> tree = createTree();
        tree.push(5, "5");
        tree.push(18, "18");
        tree.push(9, "9");
        tree.push(21, "21");
        tree.push(25, "25");
        tree.push(19, "19");
        tree.push(2, "2");
        tree.push(-4, "-4");
        tree.push(3, "3");

        tree.delete(18);
        assertEquals(tree.read(5), "5");
        assertEquals(tree.read(9), "9");
        assertEquals(tree.read(18), null);
        assertEquals(tree.read(2), "2");
        assertEquals(tree.read(-4), "-4");
        assertEquals(tree.read(21), "21");
        assertEquals(tree.read(25), "25");
        assertEquals(tree.read(19), "19");
        assertEquals(tree.read(3), "3");
        assertEquals(tree.size(), 8);
        assertEquals(tree.first(), new Tuple<>(25, "25"));
        assertEquals(tree.top(), new Tuple<>(5, "5"));
        assertEquals(tree.last(), new Tuple<>(-4, "-4"));
    }


    @Test
    public void insertDelete1() throws Exception {
        AbstractBinarySearchTree<Integer, String> tree = createTree();
        tree.push(1, "test");
        tree.delete(1);
        assertEquals(tree.read(0), null); // default key
        assertEquals(tree.read(1), null);
        assertEquals(tree.size(), 0);
        assertEquals(tree.top(), null);
        assertEquals(tree.last(), null);
        assertEquals(tree.first(), null);
    }


    @Test
    public void insertDelete2() throws Exception {
        AbstractBinarySearchTree<Integer, String> tree = createTree();
        tree.push(1, "test");
        tree.delete(1);
        tree.push(1, "example");
        assertEquals(tree.read(1), "example");
        assertEquals(tree.size(), 1);
        assertEquals(tree.top(), new Tuple<>(1, "example"));
        assertEquals(tree.last(), new Tuple<>(1, "example"));
        assertEquals(tree.first(), new Tuple<>(1, "example"));
    }


    @Test
    public void insertDelete3() throws Exception {
        AbstractBinarySearchTree<Integer, String> tree = createTree();
        tree.push(1, "test");
        tree.delete(1);
        tree.push(1, "example");
        tree.delete(1);
        assertEquals(tree.read(0), null); // default key
        assertEquals(tree.read(1), null);
        assertEquals(tree.size(), 0);
        assertEquals(tree.top(), null);
        assertEquals(tree.last(), null);
        assertEquals(tree.first(), null);
    }

    @Test
    public void deleteTop1() throws Exception {
        AbstractBinarySearchTree<Integer, String> tree = createTree();
        tree.push(1, "1");

        tree.push(3, "3");
        tree.push(2, "2");
        tree.push(5, "5");

        tree.push(-1, "-1");
        tree.push(-3, "-3");
        tree.push(-2, "-2");
        assertEquals(tree.top(), new Tuple<>(1, "1"));
        tree.delete(1);
        assertEquals(tree.read(1), null);
        assertEquals(tree.read(3), "3");
        assertEquals(tree.read(-1), "-1");
        assertEquals(tree.read(2), "2");
        assertEquals(tree.read(5), "5");
        assertEquals(tree.read(-3), "-3");
        assertEquals(tree.read(-2), "-2");
        assertEquals(tree.size(), 6);

        assertEquals(tree.top(), new Tuple<>(2, "2"));
        assertEquals(tree.last(), new Tuple<>(-3, "-3"));
        assertEquals(tree.first(), new Tuple<>(5, "5"));
    }

    void insertDelete_random(int minBound, int maxBound, int count) {
        Random r = new Random();
        AbstractBinarySearchTree<Integer, String> tree = createTree();
        List<Integer> allKeys = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            int value = r.nextInt(maxBound - minBound) + minBound;
            tree.push(value, value + "");
        }
        assertTrue(tree.size() <= count);

        for (Tuple<Integer, String> kv : tree) {
            assertEquals(kv.getKey() + "", kv.getValue());
            assertTrue(kv.getKey() >= minBound && kv.getKey() <= maxBound);
            allKeys.add(kv.getKey());
        }
        assertTrue(tree.size() <= count);
    }


    void insertDelete_range(int minBound, int maxBound) {
        AbstractBinarySearchTree<Integer, String> tree = createTree();
        for (int i = minBound; i <= maxBound; i++)
            tree.push(i, i + "");
        assertEquals(maxBound - minBound + 1, tree.size());
        assertEquals(tree.last(), new Tuple<>(minBound, minBound + ""));
        assertEquals(tree.first(), new Tuple<>(maxBound, maxBound + ""));

        for (int i = minBound; i <= maxBound; i++)
            tree.delete(i);
        assertEquals(0, tree.size());
    }

    @Test
    public void insertDelete_range_10k() throws Exception {
        insertDelete_range(0, 10_000);
    }

    @Test
    public void insertDelete_range_negative_10k() throws Exception {
        insertDelete_range(-10_000, 0);
    }

    @Test
    public void insertDelete_range_common_20k() throws Exception {
        insertDelete_range(-10_000, 10_000);
    }

    @Test
    public void insertDelete_random_10k() throws Exception {
        insertDelete_random(0, 10_000, 10000);

    }

    @Test
    public void insertDelete_random_common_20k() throws Exception {
        insertDelete_random(-10_000, 10_000, 20000);

    }
}
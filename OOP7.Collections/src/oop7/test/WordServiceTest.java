package oop7.test;


import oop7.*;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by Emil on 30.10.2016.
 */
public class WordServiceTest {
    AbstractWordService createService(String dbPath) throws IOException {
        return new WordService(Files.readAllLines(Paths.get(dbPath)), new CollectionsStub(
                new HashMap(), new PriorityQueue(), new NondirectionalGraph(), new BinarySearchTree()
        ));
    }

    AbstractWordService createService(String[] lines) {
        return new WordService(Arrays.asList(lines), new CollectionsStub(
                new HashMap(), new PriorityQueue(), new NondirectionalGraph(), new BinarySearchTree()
        ));
    }

    @Test
    public void common1_priorityQueue() {
        AbstractWordService service = createService(new String[]{
                "a", "ab", "ac", "aaaa", "c",
        });
        assertArrayEquals(service.getWordsOnPrefix("a", 5), new String[]{"a", "aaaa", "ab", "ac",});
        assertArrayEquals(service.getWordsOnPrefix("b", 5), new String[0]);
        assertArrayEquals(service.getWordsOnPrefix("c", 5), new String[]{"c"});
        assertArrayEquals(service.getWordsOnPrefix("d", 5), new String[0]);
    }


    @Test
    public void common1_bst() {
        AbstractWordService service = new WordService(
                Arrays.asList("a", "ab", "ac", "aaaa", "c"),
                new CollectionsStub(new HashMap(), new BinarySearchTree(), new NondirectionalGraph(), new BinarySearchTree())
        );
        assertArrayEquals(service.getWordsOnPrefix("a", 5), new String[]{"a", "aaaa", "ab", "ac",});
        assertArrayEquals(service.getWordsOnPrefix("b", 5), new String[0]);
        assertArrayEquals(service.getWordsOnPrefix("c", 5), new String[]{"c"});
        assertArrayEquals(service.getWordsOnPrefix("d", 5), new String[0]);
    }


    @Test
    public void english1k() throws IOException {
        AbstractWordService service = createService("words.txt");
        assertArrayEquals(service.getWordsOnPrefix("a", 100), new String[] {
                "a", "able", "about", "above", "act", "add", "afraid", "after", "again",
                "against", "age", "ago", "agree", "air", "all", "allow", "also", "always", "am", "among",
                "an", "and", "anger", "animal", "answer", "any", "appear", "apple", "are", "area",
                "arm", "arrange", "arrive", "art", "as", "ask", "at", "atom"
        });
        assertArrayEquals(service.getWordsOnPrefix("b", 100), new String[] {
                "baby", "back", "bad", "ball", "band", "bank", "bar", "base", "basic", "bat",
                "be", "bear", "beat", "beauty", "bed", "been", "before", "began", "begin",
                "behind", "believe", "bell", "best", "better", "between", "big", "bird", "bit",
                "black", "block", "blood", "blow", "blue", "board", "boat", "body", "bone", "book",
                "born", "both", "bottom", "bought", "box", "boy", "branch", "bread", "break",
                "bright", "bring", "broad", "broke", "brother", "brought", "brown", "build", "burn",
                "busy", "but", "buy", "by"
        });
    }


    @Test
    public void english1k_shuffled() throws IOException {
        AbstractWordService service = createService("words_shuffled.txt");
        assertArrayEquals(service.getWordsOnPrefix("a", 100), new String[] {
                "a", "able", "about", "above", "act", "add", "afraid", "after", "again",
                "against", "age", "ago", "agree", "air", "all", "allow", "also", "always", "am", "among",
                "an", "and", "anger", "animal", "answer", "any", "appear", "apple", "are", "area",
                "arm", "arrange", "arrive", "art", "as", "ask", "at", "atom"
        });
        assertArrayEquals(service.getWordsOnPrefix("b", 100), new String[] {
                "baby", "back", "bad", "ball", "band", "bank", "bar", "base", "basic", "bat",
                "be", "bear", "beat", "beauty", "bed", "been", "before", "began", "begin",
                "behind", "believe", "bell", "best", "better", "between", "big", "bird", "bit",
                "black", "block", "blood", "blow", "blue", "board", "boat", "body", "bone", "book",
                "born", "both", "bottom", "bought", "box", "boy", "branch", "bread", "break",
                "bright", "bring", "broad", "broke", "brother", "brought", "brown", "build", "burn",
                "busy", "but", "buy", "by"
        });
    }


    @Test
    public void english1k_shuffled_bst() throws IOException {
        AbstractWordService service  = new WordService(
                Files.readAllLines(Paths.get("words_shuffled.txt")),
                new CollectionsStub(new HashMap(), new BinarySearchTree(), new NondirectionalGraph(), new BinarySearchTree())
        );
        assertArrayEquals(service.getWordsOnPrefix("a", 100), new String[] {
                "a", "able", "about", "above", "act", "add", "afraid", "after", "again",
                "against", "age", "ago", "agree", "air", "all", "allow", "also", "always", "am", "among",
                "an", "and", "anger", "animal", "answer", "any", "appear", "apple", "are", "area",
                "arm", "arrange", "arrive", "art", "as", "ask", "at", "atom"
        });
        assertArrayEquals(service.getWordsOnPrefix("b", 100), new String[] {
                "baby", "back", "bad", "ball", "band", "bank", "bar", "base", "basic", "bat",
                "be", "bear", "beat", "beauty", "bed", "been", "before", "began", "begin",
                "behind", "believe", "bell", "best", "better", "between", "big", "bird", "bit",
                "black", "block", "blood", "blow", "blue", "board", "boat", "body", "bone", "book",
                "born", "both", "bottom", "bought", "box", "boy", "branch", "bread", "break",
                "bright", "bring", "broad", "broke", "brother", "brought", "brown", "build", "burn",
                "busy", "but", "buy", "by"
        });
    }
}

package oop7.test;


import oop7.Edge;
import oop7.NondirectionalGraph;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Created by Emil on 23.10.2016.
 */
public class NondirectionalGraphTest {
    public static HashSet<Edge<Integer>> getEdges(NondirectionalGraph<Integer> graph) {
        List<Edge<Integer>> edges = new ArrayList<>();
        for (Edge<Integer> edge : graph)
            edges.add(edge);
        return new HashSet<>(edges);
    }

    public static Edge<Integer> createEdge(int a, int b) {
        return new Edge<>(a, b);
    }

    @Test
    public void add_common() throws Exception {
        NondirectionalGraph<Integer> graph = new NondirectionalGraph<>();
        graph.add(2);
        graph.add(3);
        assertFalse(graph.isEmpty());
    }

    @Test
    public void add_oneElement() throws Exception {
        NondirectionalGraph<Integer> graph = new NondirectionalGraph<>();
        graph.add(2);
        assertFalse(graph.isEmpty());
    }

    @Test
    public void addEdge_onePair() throws Exception {
        NondirectionalGraph<Integer> graph = new NondirectionalGraph<>();
        graph.add(2);
        graph.add(3);
        graph.addEdge(2, 3);
        assertEquals(getEdges(graph), new HashSet<Edge<Integer>>() {{
            add(createEdge(2, 3));
        }});
        assertEquals(getEdges(graph), new HashSet<Edge<Integer>>() {{
            add(createEdge(3, 2));
        }});
        assertFalse(graph.isEmpty());
    }

    @Test
    public void addEdge_twoPair1() throws Exception {
        NondirectionalGraph<Integer> graph = new NondirectionalGraph<>();
        graph.add(2);
        graph.add(3);
        graph.add(4);
        graph.addEdge(2, 3);
        graph.addEdge(2, 4);
        assertEquals(getEdges(graph), new HashSet<Edge<Integer>>() {{
            add(createEdge(2, 3));
            add(createEdge(4, 2));
        }});
        assertFalse(graph.isEmpty());
    }

    @Test
    public void addEdge_twoPair2() throws Exception {
        NondirectionalGraph<Integer> graph = new NondirectionalGraph<>();
        graph.add(2);
        graph.add(3);
        graph.add(4);
        graph.add(5);
        graph.addEdge(2, 3);
        graph.addEdge(5, 4);
        assertEquals(getEdges(graph), new HashSet<Edge<Integer>>() {{
            add(createEdge(3, 2));
            add(createEdge(4, 5));
        }});
        assertFalse(graph.isEmpty());
    }

    @Test
    public void addEdge_twoPair_invalidEdge1() throws Exception {
        NondirectionalGraph<Integer> graph = new NondirectionalGraph<>();
        graph.add(2);
        graph.add(3);
        graph.add(4);
        graph.add(5);
        graph.addEdge(2, 6);
        graph.addEdge(5, 4);
        assertEquals(getEdges(graph), new HashSet<Edge<Integer>>() {{
            add(createEdge(4, 5));
            add(createEdge(6, 2));
        }});
        assertFalse(graph.isEmpty());
    }

    @Test
    public void addEdge_threePair1() throws Exception {
        NondirectionalGraph<Integer> graph = new NondirectionalGraph<>();
        graph.add(2);
        graph.add(3);
        graph.add(4);
        graph.add(5);
        graph.addEdge(2, 4);
        graph.addEdge(3, 4);
        graph.addEdge(5, 4);
        assertEquals(getEdges(graph), new HashSet<Edge<Integer>>() {{
            add(createEdge(4, 5));
            add(createEdge(3, 4));
            add(createEdge(2, 4));
        }});
        assertFalse(graph.isEmpty());
    }


    @Test
    public void deleteEdge_common1() throws Exception {
        NondirectionalGraph<Integer> graph = new NondirectionalGraph<>();
        graph.add(2);
        graph.add(3);
        graph.add(4);
        graph.add(5);
        graph.addEdge(2, 4);
        graph.addEdge(3, 4);
        graph.addEdge(5, 4);
        assertEquals(getEdges(graph), new HashSet<Edge<Integer>>() {{
            add(createEdge(4, 5));
            add(createEdge(3, 4));
            add(createEdge(2, 4));
        }});
        graph.deleteEdge(4, 5);
        assertEquals(getEdges(graph), new HashSet<Edge<Integer>>() {{
            add(createEdge(3, 4));
            add(createEdge(4, 2));
        }});
        assertFalse(graph.isEmpty());
    }


    @Test
    public void deleteEdge_invalidEdge1() throws Exception {
        NondirectionalGraph<Integer> graph = new NondirectionalGraph<>();
        graph.add(2);
        graph.add(3);
        graph.add(4);
        graph.add(5);
        graph.addEdge(2, 4);
        graph.addEdge(3, 4);
        graph.addEdge(5, 4);
        assertEquals(getEdges(graph), new HashSet<Edge<Integer>>() {{
            add(createEdge(4, 5));
            add(createEdge(3, 4));
            add(createEdge(2, 4));
        }});
        graph.deleteEdge(4, 4);
        assertEquals(getEdges(graph), new HashSet<Edge<Integer>>() {{
            add(createEdge(4, 5));
            add(createEdge(3, 4));
            add(createEdge(4, 2));
        }});
        assertFalse(graph.isEmpty());
    }

    @Test
    public void deleteEdge_invalidEdge2() throws Exception {
        NondirectionalGraph<Integer> graph = new NondirectionalGraph<>();
        graph.add(2);
        graph.add(3);
        graph.add(4);
        graph.add(5);
        graph.addEdge(2, 4);
        graph.addEdge(3, 4);
        graph.addEdge(5, 4);
        assertEquals(getEdges(graph), new HashSet<Edge<Integer>>() {{
            add(createEdge(4, 5));
            add(createEdge(3, 4));
            add(createEdge(2, 4));
        }});
        graph.deleteEdge(4, 6);
        assertEquals(getEdges(graph), new HashSet<Edge<Integer>>() {{
            add(createEdge(4, 5));
            add(createEdge(3, 4));
            add(createEdge(4, 2));
        }});
        assertFalse(graph.isEmpty());
    }

    @Test
    public void deleteEdge_10k() throws Exception {
        NondirectionalGraph<Integer> graph = new NondirectionalGraph<>();
        for (int i = 0; i < 10_000; i++) {
            graph.addEdge(i, i + 1);
        }
        for (int i = 0; i < 10_000; i++) {
            graph.deleteEdge(i, i + 1);
        }
        assertFalse(graph.isEmpty());
    }

}
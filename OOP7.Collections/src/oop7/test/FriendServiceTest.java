package oop7.test;


import oop7.*;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;

/**
 * Created by Emil on 30.10.2016.
 */
public class FriendServiceTest {
    AbstractFriendService createService(String dbPath) throws IOException {
        return new FriendService(Files.readAllLines(Paths.get(dbPath)), new CollectionsStub(
                new HashMap(), new PriorityQueue(), new NondirectionalGraph(), new BinarySearchTree()
        ));
    }

    AbstractFriendService createService(String[] db) {
        return new FriendService(Arrays.asList(db), new CollectionsStub(
                new HashMap(), new PriorityQueue(), new NondirectionalGraph(), new BinarySearchTree()
        ));
    }

    public void assertSetEquals(String[] a, String[] b) {
        assertEquals(new HashSet(Arrays.asList(a)), new HashSet(Arrays.asList(b)));
    }

    @Test
    public void getFriends_noSuchName() {
        AbstractFriendService service = createService(new String[]{
                "vova - nikolay", "olga - lena", "sasha - dima"
        });

        assertSetEquals(service.getPossibleFriends("noname", 1), new String[0]);
        assertSetEquals(service.getPossibleFriends("noname2", 2), new String[0]);
    }

    @Test
    public void getFriends_noSuggestions() {
        AbstractFriendService service = createService(new String[]{
                "vova - nikolay", "olga - lena", "sasha - dima", "sveta - vova"
        });
        assertSetEquals(service.getPossibleFriends("sasha", 1), new String[0]);
        assertSetEquals(service.getPossibleFriends("dima", 1), new String[0]);
        assertSetEquals(service.getPossibleFriends("olga", 2), new String[0]);
    }

    @Test
    public void getFriends_oneSuggestion() {
        AbstractFriendService service = createService(new String[]{
                "vova - nikolay", "olga - lena", "sasha - dima", "sveta - vova"
        });
        assertSetEquals(service.getPossibleFriends("nikolay", 1), new String[]{"sveta"});
        assertSetEquals(service.getPossibleFriends("sveta", 2), new String[]{"nikolay"});
    }


    @Test
    public void getFriends_multipleSuggestions1() {
        AbstractFriendService service = createService(new String[]{
                "vova - nikolay", "vova - serega", "sanya - vova", "sveta - vova"
        });
        assertSetEquals(service.getPossibleFriends("sveta", 10), new String[]{
                "sanya", "serega", "nikolay"});
        assertSetEquals(service.getPossibleFriends("sanya", 3), new String[]{
                "sveta", "serega", "nikolay"});
        assertSetEquals(service.getPossibleFriends("serega", 4), new String[]{
                "sveta", "sanya", "nikolay"});
    }

}

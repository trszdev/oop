package oop7;

/**
 * Created by Emil on 17.10.2016.
 */
public interface AbstractWordService {

    String[] getWordsOnPrefix(String prefix, int amount);
}

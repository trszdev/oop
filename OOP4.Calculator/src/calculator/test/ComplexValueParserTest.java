package calculator.test;

import calculator.ParseValueException;
import calculator.datatypes.complex.ComplexValue;
import calculator.datatypes.complex.ComplexValueParser;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by Emil on 18.09.2016.
 */
public class ComplexValueParserTest extends ParserTestBase {
    public ComplexValueParserTest() {
        super(new ComplexValueParser());
    }

    void test_complexValue(String value, double expectedReal, double expectedImage)
            throws ParseValueException {
        ComplexValue val = (ComplexValue) parse(value);
        assertEquals(expectedImage, val.getImage(), .0000000001);
        assertEquals(expectedReal, val.getReal(), .0000000001);
    }

    @Test
    public void parse_common1() throws Exception {
        test_complexValue("1+2i", 1, 2);
    }

    @Test
    public void parse_common2() throws Exception {
        test_complexValue("-1+2i", -1, 2);
    }

    @Test
    public void parse_common3() throws Exception {
        test_complexValue("-1+-2i", -1, -2);
    }

    @Test
    public void parse_common4() throws Exception {
        test_complexValue("0+2i", 0, 2);
    }

    @Test
    public void parse_common5() throws Exception {
        test_complexValue("-1+2i", -1, 2);
    }

    @Test
    public void parse_bad1() throws Exception {
        try {
            test_complexValue("+", -1, 2);
        } catch (Exception e) {
            return;
        }
        fail();
    }

    @Test
    public void parse_bad2() throws Exception {
        try {
            test_complexValue("-", -1, 2);
        } catch (Exception e) {
            return;
        }
        fail();
    }

    @Test
    public void parse_bad3() throws Exception {
        try {
            test_complexValue("55", -1, 2);
        } catch (Exception e) {
            return;
        }
        fail();
    }

    @Test
    public void parse_bad4() throws Exception {
        try {
            test_complexValue("55", -1, 2);
        } catch (Exception e) {
            return;
        }
        fail();
    }
}
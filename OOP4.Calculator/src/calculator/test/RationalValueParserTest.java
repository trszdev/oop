package calculator.test;

import calculator.ParseValueException;
import calculator.datatypes.rational.RationalValue;
import calculator.datatypes.rational.RationalValueParser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by Emil on 18.09.2016.
 */
public class RationalValueParserTest extends ParserTestBase {
    public RationalValueParserTest() {
        super(new RationalValueParser());
    }

    void test_rationalValue(String value, int expectedNumerator, int expectedDenominator)
            throws ParseValueException {
        RationalValue val = (RationalValue) parse(value);
        assertEquals(expectedNumerator, val.getNumerator());
        assertEquals(expectedDenominator, val.getDenominator());
    }

    @org.junit.Test
    public void parse_common1() throws Exception {
        test_rationalValue("1/4", 1, 4);
    }

    @org.junit.Test
    public void parse_common2() throws Exception {
        test_rationalValue("1/3", 1, 3);
    }

    @org.junit.Test
    public void parse_common3() throws Exception {
        test_rationalValue("2/6", 2, 6);
    }

    @org.junit.Test
    public void parse_common4() throws Exception {
        test_rationalValue("0/6", 0, 6);
    }

    @org.junit.Test
    public void parse_common5() throws Exception {
        test_rationalValue("0/1", 0, 1);
    }

    @org.junit.Test
    public void parse_common6() throws Exception {
        test_rationalValue("-1/1", -1, 1);
    }

    @org.junit.Test
    public void parse_common7() throws Exception {
        test_rationalValue("-5/1", -5, 1);
    }

    @org.junit.Test
    public void parse_zero_denominator1() throws Exception {
        try {
            test_rationalValue("1/0", 1, 0);
        } catch (Exception e) {
            return;
        }
        fail();
    }

    @org.junit.Test
    public void parse_zero_denominator2() throws Exception {
        try {
            test_rationalValue("0/0", 0, 0);
        } catch (Exception e) {
            return;
        }
        fail();
    }

    @org.junit.Test
    public void parse_zero_denominator3() throws Exception {
        try {
            test_rationalValue("-8/0", -8, 0);
        } catch (Exception e) {
            return;
        }
        fail();
    }


    @org.junit.Test
    public void parse_bad1() throws Exception {
        try {
            test_rationalValue("-", -8, 0);
        } catch (Exception e) {
            return;
        }
        fail();
    }

    @org.junit.Test
    public void parse_bad2() throws Exception {
        try {
            test_rationalValue("3", -8, 0);
        } catch (Exception e) {
            return;
        }
        fail();
    }

    @org.junit.Test
    public void parse_bad3() throws Exception {
        try {
            test_rationalValue("/", -8, 0);
        } catch (Exception e) {
            return;
        }
        fail();
    }
}
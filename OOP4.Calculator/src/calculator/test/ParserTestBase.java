package calculator.test;

import calculator.AbstractValue;
import calculator.AbstractValueParser;
import calculator.ParseValueException;

import static org.junit.Assert.assertEquals;

/**
 * Created by Emil on 20.09.2016.
 */
public abstract class ParserTestBase {
    protected final AbstractValueParser valueParser;

    public ParserTestBase(AbstractValueParser valueParser) {
        this.valueParser = valueParser;
    }

    protected final AbstractValue parse(String value)
            throws ParseValueException {
        AbstractValue result = valueParser.parse(value);
        assertEquals(value, result.toString());
        return result;
    }
}

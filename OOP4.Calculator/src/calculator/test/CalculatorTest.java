package calculator.test;

import calculator.*;
import calculator.datatypes.rational.RationalValue;
import calculator.datatypes.rational.RationalValueParser;
import calculator.operation.OperationNotSupportedException;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by Emil on 20.09.2016.
 */
// testing on rational values
public class CalculatorTest {
    public final AbstractValueParser parser = new RationalValueParser();
    public final AbstractEvaluator calc = new Calculator(parser);

    // since 1/3 and 3/9 are equal, we should compare value diffs
    void test(String input, String expectedOutput)
            throws DivisionByZeroException, OperationNotSupportedException, ParseValueException {
        RationalValue expectedValue = (RationalValue) parser.parse(expectedOutput);
        RationalValue outputValue = (RationalValue) parser.parse(calc.evaluate(input));
        RationalValue diff = (RationalValue) expectedValue.sub(outputValue);
        assertTrue(String.format("Expected: %s but found %s", expectedValue, outputValue),
                0 == diff.getNumerator());
    }

    void testShouldThrow(String input) {
        try {
            calc.evaluate(input);
        } catch (ParseValueException e) {
            e.printStackTrace();
            return;
        } catch (OperationNotSupportedException e) {
            e.printStackTrace();
            return;
        } catch (DivisionByZeroException e) {
            e.printStackTrace();
            return;
        }
        fail();
    }

    @Test
    public void evaluate_nobrackets1() throws Exception {
        test("1/3 + 2/3", "3/3");
    }

    @Test
    public void evaluate_nobrackets2() throws Exception {
        test("1/3 + 2/4", "5/6");
    }

    @Test
    public void evaluate_nobrackets3() throws Exception {
        test("1/3 + 1/3 + 1/3 + 1/3 - 1/3 + 1/2", "15/10");
    }


    @Test
    public void evaluate_nobrackets4() throws Exception {
        test("1/3 * 1/3 + 1/3 - 2/9", "2/9");
    }

    @Test
    public void evaluate_nobrackets5() throws Exception {
        test("1/3 / 1/3 * 10/1", "10/1");
    }

    @Test
    public void evaluate_nobrackets6() throws Exception {
        testShouldThrow("1/3 / ");
    }

    @Test
    public void evaluate_nobrackets7() throws Exception {
        testShouldThrow("* 1/3");
    }

    @Test
    public void evaluate_nobrackets8() throws Exception {
        test("-1/3 - -1/3", "0/1");
    }

    @Test
    public void evaluate_brackets1() throws Exception {
        testShouldThrow(")");
    }

    @Test
    public void evaluate_brackets2() throws Exception {
        testShouldThrow("(");
    }

    @Test
    public void evaluate_brackets3() throws Exception {
        testShouldThrow("( )");
    }

    @Test
    public void evaluate_brackets4() throws Exception {
        testShouldThrow("( ( )");
    }

    @Test
    public void evaluate_brackets5() throws Exception {
        test("( 1/3 )", "1/3");
    }

    @Test
    public void evaluate_brackets6() throws Exception {
        test("( 1/3 + 2/3 )", "1/1");
    }

    @Test
    public void evaluate_brackets7() throws Exception {
        test("( 1/3 + 2/3 ) * 3/1", "3/1");
    }

    @Test
    public void evaluate_brackets8() throws Exception {
        test("1/3 + ( 2/3 * 3/1 )", "7/3");
    }

    @Test
    public void evaluate_brackets9() throws Exception {
        test("( 2/7 * 1/8 ) + ( 2/3 * 3/1 )", "57/28");
    }


    @Test
    public void evaluate_brackets10() throws Exception {
        test("( 1/2 + 2/2 ) * ( 5/1 - 3/1 )", "3/1");
    }
}
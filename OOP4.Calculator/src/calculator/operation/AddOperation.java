package calculator.operation;

import calculator.AbstractValue;

/**
 * Created by Emil on 19.09.2016.
 */
public class AddOperation implements AbstractOperation {
    public static final AddOperation instance = new AddOperation();

    @Override
    public String getName() {
        return "+";
    }

    @Override
    public int getPriority() {
        return 1;
    }

    @Override
    public AbstractValue execute(AbstractValue right, AbstractValue left)
            throws OperationNotSupportedException {
        return right.add(left);
    }
}

package calculator.operation;

import calculator.AbstractValue;

/**
 * Created by Emil on 19.09.2016.
 */
public class SubOperation implements AbstractOperation {
    public static final SubOperation instance = new SubOperation();

    @Override
    public String getName() {
        return "-";
    }

    @Override
    public int getPriority() {
        return 1;
    }

    @Override
    public AbstractValue execute(AbstractValue right, AbstractValue left)
            throws OperationNotSupportedException {
        return right.sub(left);
    }
}

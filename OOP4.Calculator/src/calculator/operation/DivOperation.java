package calculator.operation;

import calculator.AbstractValue;
import calculator.DivisionByZeroException;

/**
 * Created by Emil on 19.09.2016.
 */
public class DivOperation implements AbstractOperation {
    public static final DivOperation instance = new DivOperation();

    @Override
    public String getName() {
        return "/";
    }

    @Override
    public int getPriority() {
        return 2;
    }

    @Override
    public AbstractValue execute(AbstractValue right, AbstractValue left)
            throws OperationNotSupportedException, DivisionByZeroException {
        return right.div(left);
    }
}

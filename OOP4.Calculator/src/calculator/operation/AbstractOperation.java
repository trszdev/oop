package calculator.operation;

import calculator.AbstractValue;
import calculator.DivisionByZeroException;

/**
 * Created by Emil on 19.09.2016.
 */
public interface AbstractOperation {
    String getName();

    int getPriority();

    AbstractValue execute(AbstractValue right, AbstractValue left)
            throws OperationNotSupportedException, DivisionByZeroException;
}

package calculator.operation;

/**
 * Created by Emil on 20.09.2016.
 */
public final class BasicOperations {
    public static final AbstractOperation[] array = new AbstractOperation[]{
            new AddOperation(), new SubOperation(), new MulOperation(), new DivOperation()
    };
}

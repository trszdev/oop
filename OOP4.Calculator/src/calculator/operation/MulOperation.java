package calculator.operation;

import calculator.AbstractValue;

/**
 * Created by Emil on 19.09.2016.
 */
public class MulOperation implements AbstractOperation {
    public static final MulOperation instance = new MulOperation();

    @Override
    public String getName() {
        return "*";
    }

    @Override
    public int getPriority() {
        return 2;
    }

    @Override
    public AbstractValue execute(AbstractValue right, AbstractValue left)
            throws OperationNotSupportedException {
        return right.mul(left);
    }
}

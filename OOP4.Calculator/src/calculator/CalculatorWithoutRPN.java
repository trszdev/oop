package calculator;

import calculator.operation.AbstractOperation;
import calculator.operation.OperationNotSupportedException;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Emil on 19.09.2016.
 */
public class CalculatorWithoutRPN implements AbstractEvaluator {
    protected final AbstractValueParser valueParser;
    protected final Map<String, AbstractOperation> operations;

    public CalculatorWithoutRPN(AbstractValueParser valueParser) {
        this.valueParser = valueParser;
        this.operations = new HashMap<String, AbstractOperation>();
        for (AbstractOperation operation : valueParser.getOperations()) {
            this.operations.put(operation.getName(), operation);
        }
    }


    @Override
    public String evaluate(String expression)
            throws ParseValueException, OperationNotSupportedException, DivisionByZeroException {
        Scanner scanner = new Scanner(expression);
        AbstractValue right = valueParser.parse(scanner.next());
        String operationToken = scanner.next();
        AbstractOperation op = operations.get(operationToken);
        if (op == null) throw new OperationNotSupportedException(operationToken);
        AbstractValue left = valueParser.parse(scanner.next());
        return op.execute(right, left).toString();
    }
}

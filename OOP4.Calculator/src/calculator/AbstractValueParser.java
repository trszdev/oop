package calculator;

import calculator.operation.AbstractOperation;

public interface AbstractValueParser {
    AbstractValue parse(String value) throws ParseValueException;

    AbstractOperation[] getOperations();

    String getDatatypeName();
}

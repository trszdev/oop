package calculator;

import calculator.operation.OperationNotSupportedException;

/**
 * Created by Emil on 19.09.2016.
 */
public interface AbstractEvaluator {
    String evaluate(String expression) throws ParseValueException, OperationNotSupportedException, DivisionByZeroException;
}

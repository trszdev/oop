package calculator.console;

import calculator.AbstractEvaluator;
import calculator.AbstractValueParser;
import calculator.Calculator;
import calculator.datatypes.ValueParsers;

import java.util.Scanner;

public class Program {

    private final Scanner scanner;

    private final AbstractEvaluator calc;

    private AbstractValueParser[] valueParsers = ValueParsers.array;

    public Program() {
        scanner = new Scanner(System.in);
        AbstractValueParser parser = inputValueParser();
        System.out.println("Работаем с типом '" + parser.getDatatypeName()
                + "'");
        calc = new Calculator(parser);
        scanner.nextLine();
    }

    public static void main(String args[]) {
        try {
            Program instance = new Program();
            instance.run(args);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    private AbstractValueParser inputValueParser() {
        showChoises();
        int choise = scanner.nextInt();
        if (choise >= 1 && choise <= valueParsers.length) //choise )))))))))))))))) cyka
            return valueParsers[choise - 1];
        else {
            System.out.println("Неверный выбор!");
            return inputValueParser();
        }
    }

    private void showChoises() {
        System.out.println("Вам нужно выбрать тип данных. Возможные варианты:");
        for (int i = 0; i < valueParsers.length; i++)
            System.out.println("  " + (i + 1) + ". "
                    + valueParsers[i].getDatatypeName());
    }

    private void run(String[] args) {
        while (true) {
            String line = scanner.nextLine();
            if ("exit".equals(line))
                break;
            try {
                System.out.println(" = " + calc.evaluate(line));
            } catch (Exception exception) {
                System.out.println(String.format("Возникла ошибка: %s", exception));
            }
        }
    }

}

package calculator.datatypes;

import calculator.AbstractValueParser;
import calculator.datatypes.complex.ComplexValueParser;
import calculator.datatypes.integer.IntegerValueParser;
import calculator.datatypes.rational.RationalValueParser;
import calculator.datatypes.real.RealValueParser;

/**
 * Created by Emil on 20.09.2016.
 */
public final class ValueParsers {
    public final static AbstractValueParser[] array = new AbstractValueParser[]{
            new RationalValueParser(),
            new IntegerValueParser(),
            new ComplexValueParser(),
            new RealValueParser(),
    };
}

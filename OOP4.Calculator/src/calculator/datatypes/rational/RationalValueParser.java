package calculator.datatypes.rational;

import calculator.AbstractValue;
import calculator.AbstractValueParser;
import calculator.ParseValueException;
import calculator.operation.AbstractOperation;
import calculator.operation.BasicOperations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Emil on 18.09.2016.
 */
public class RationalValueParser implements AbstractValueParser {
    protected static final Pattern rationalValueParser
            = Pattern.compile("(\\-?\\d+)\\/(\\d+)");

    @Override
    public AbstractValue parse(String value) throws ParseValueException {
        Matcher matcher = rationalValueParser.matcher(value);
        if (!matcher.find()) throw new ParseValueException();
        int numerator = Integer.parseInt(matcher.group(1));
        int denominator = Integer.parseInt(matcher.group(2));
        return new RationalValue(numerator, denominator);
    }

    @Override
    public AbstractOperation[] getOperations() {
        return BasicOperations.array;
    }

    @Override
    public String getDatatypeName() {
        return "рациональные числа";
    }
}

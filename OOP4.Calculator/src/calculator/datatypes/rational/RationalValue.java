package calculator.datatypes.rational;

import calculator.AbstractValue;
import calculator.DivisionByZeroException;
import calculator.operation.OperationNotSupportedException;

/**
 * Created by Emil on 18.09.2016.
 */
public class RationalValue extends AbstractValue {
    protected final int numerator;
    protected final int denominator;

    public RationalValue(int numerator, int denominator) {
        if (denominator == 0)
            throw new IllegalArgumentException("Denominator is zero");

        this.numerator = denominator < 0 ? -numerator : numerator;
        this.denominator = Math.abs(denominator);
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    @Override
    public AbstractValue add(AbstractValue operand) throws OperationNotSupportedException {
        RationalValue value = (RationalValue) operand;
        int newNumerator = this.numerator * value.denominator + value.numerator * this.denominator;
        int newDenominator = this.denominator * value.denominator;
        return new RationalValue(newNumerator, newDenominator);
    }

    @Override
    public AbstractValue sub(AbstractValue operand) throws OperationNotSupportedException {
        RationalValue value = (RationalValue) operand;
        int newNumerator = this.numerator * value.denominator - value.numerator * this.denominator;
        int newDenominator = this.denominator * value.denominator;
        return new RationalValue(newNumerator, newDenominator);
    }

    @Override
    public AbstractValue mul(AbstractValue operand) throws OperationNotSupportedException {
        RationalValue value = (RationalValue) operand;
        int newNumerator = this.numerator * value.numerator;
        int newDenominator = this.denominator * value.denominator;
        return new RationalValue(newNumerator, newDenominator);
    }

    @Override
    public AbstractValue div(AbstractValue operand) throws DivisionByZeroException, OperationNotSupportedException {
        RationalValue value = (RationalValue) operand;
        int newNumerator = this.numerator * value.denominator;
        int newDenominator = this.denominator * value.numerator;
        return new RationalValue(newNumerator, newDenominator);
    }

    @Override
    public String toString() {
        return String.format("%d/%d", numerator, denominator);
    }
}

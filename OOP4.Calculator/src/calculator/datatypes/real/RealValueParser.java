package calculator.datatypes.real;

import calculator.AbstractValue;
import calculator.AbstractValueParser;
import calculator.ParseValueException;
import calculator.operation.AbstractOperation;
import calculator.operation.BasicOperations;

public class RealValueParser implements AbstractValueParser {

    public AbstractValue parse(String value) throws ParseValueException {
        try {
            return new RealValue(Double.parseDouble(value));
        } catch (NumberFormatException exception) {
            throw new ParseValueException();
        }
    }

    @Override
    public AbstractOperation[] getOperations() {
        return BasicOperations.array;
    }

    public String getDatatypeName() {
        return "вещественные числа";
    }


}

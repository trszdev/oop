package calculator.datatypes.complex;

import calculator.AbstractValue;
import calculator.AbstractValueParser;
import calculator.ParseValueException;
import calculator.operation.AbstractOperation;
import calculator.operation.BasicOperations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Emil on 18.09.2016.
 */
// TODO: formatting
public class ComplexValueParser implements AbstractValueParser {
    protected static final Pattern complexValuePattern
            = Pattern.compile("(\\-?\\d+\\.?\\d*)\\+(\\-?\\d+\\.?\\d*)i");


    @Override
    public AbstractValue parse(String value) throws ParseValueException {
        Matcher matcher = complexValuePattern.matcher(value);
        if (!matcher.find()) throw new ParseValueException();
        return new ComplexValue(Double.parseDouble(matcher.group(1)),
                Double.parseDouble(matcher.group(2)));
    }

    @Override
    public AbstractOperation[] getOperations() {
        return BasicOperations.array;
    }

    @Override
    public String getDatatypeName() {
        return "комплексные числа";
    }
}

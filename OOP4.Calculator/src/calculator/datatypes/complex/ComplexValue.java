package calculator.datatypes.complex;

import calculator.AbstractValue;
import calculator.DivisionByZeroException;
import calculator.operation.OperationNotSupportedException;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by Emil on 18.09.2016.
 */
// TODO: formatting
public class ComplexValue extends AbstractValue {
    protected static NumberFormat nf = new DecimalFormat("#.####");
    protected final double real;
    protected final double image;

    public ComplexValue(double real, double image) {
        this.real = real;
        this.image = image;
    }

    public double getImage() {
        return image;
    }

    public double getReal() {
        return real;
    }

    @Override
    public AbstractValue add(AbstractValue operand) throws OperationNotSupportedException {
        ComplexValue value = (ComplexValue) operand;
        return new ComplexValue(this.real + value.real, this.image + value.image);
    }

    @Override
    public AbstractValue sub(AbstractValue operand) throws OperationNotSupportedException {
        ComplexValue value = (ComplexValue) operand;
        return new ComplexValue(this.real - value.real, this.image - value.image);
    }

    @Override
    public AbstractValue mul(AbstractValue operand) throws OperationNotSupportedException {
        ComplexValue value = (ComplexValue) operand;
        double newReal = this.real * value.real - this.image * value.image;
        double newImage = this.real * value.image + this.image * value.real;
        return new ComplexValue(newReal, newImage);
    }

    @Override
    public AbstractValue div(AbstractValue operand) throws DivisionByZeroException, OperationNotSupportedException {
        double scale = this.real * this.real + this.image * this.image;
        return this.mul(new ComplexValue(real / scale, -image / scale));
    }

    @Override
    public String toString() {
        return String.format("%s+%si", nf.format(real), nf.format(image));
    }
}

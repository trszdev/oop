package calculator.datatypes.integer;

import calculator.AbstractValue;
import calculator.AbstractValueParser;
import calculator.ParseValueException;
import calculator.operation.AbstractOperation;
import calculator.operation.BasicOperations;

public class IntegerValueParser implements AbstractValueParser {

    public AbstractValue parse(String value) throws ParseValueException {
        try {
            return new IntegerValue(Integer.parseInt(value));
        } catch (NumberFormatException exception) {
            throw new ParseValueException();
        }
    }

    @Override
    public AbstractOperation[] getOperations() {
        return BasicOperations.array;
    }

    public String getDatatypeName() {
        return "целые числа";
    }

}

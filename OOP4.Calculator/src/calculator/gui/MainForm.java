package calculator.gui;

import calculator.AbstractEvaluator;
import calculator.AbstractValueParser;
import calculator.Calculator;
import calculator.datatypes.ValueParsers;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Emil on 19.09.2016.
 */

// TODO: refactor
public class MainForm {
    static Dimension windowSize = new Dimension(480, 200);
    static String windowTitle = "OOP4.Calculator";
    static AbstractValueParser[] parsers = ValueParsers.array;
    static AbstractEvaluator calc;
    static JTextField expressionField = new JTextField("1/3 + 2/3");
    static JTextField resultField = new JTextField("42");
    static JButton calcButton = new JButton("Calc!");
    static JComboBox datatypesCombobox = new JComboBox();

    static void RefreshCalc() {
        calc = new Calculator(parsers[datatypesCombobox.getSelectedIndex()]);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(() -> createAndShowGUI());
    }

    static JFrame createFrame(JPanel panel) {
        JFrame frame = new JFrame(windowTitle);
        frame.setSize(windowSize);
        frame.setMaximumSize(windowSize);
        frame.setMinimumSize(windowSize);
        frame.setResizable(false);
        frame.setLocation(100, 120);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(panel);
        frame.pack();
        return frame;
    }


    static void createAndShowGUI() {
        for (AbstractValueParser parser : parsers) {
            datatypesCombobox.addItem(parser.getDatatypeName());
        }

        datatypesCombobox.addActionListener(e -> RefreshCalc());

        calcButton.addActionListener(e -> {
            String expression = expressionField.getText();
            try {
                resultField.setText(calc.evaluate(expression));
            } catch (Exception e1) {
                JOptionPane.showMessageDialog(null, expression,
                        e1.toString(), JOptionPane.ERROR_MESSAGE);
            }
        });

        calcButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
        resultField.setEditable(false);
        datatypesCombobox.setPreferredSize(new Dimension(windowSize.width - 20, 30));
        expressionField.setPreferredSize(new Dimension(windowSize.width - 20, 30));
        resultField.setPreferredSize(new Dimension(windowSize.width - 20, 30));


        JPanel panel = new JPanel(new WrapLayout());
        panel.add(datatypesCombobox);
        panel.add(expressionField);
        panel.add(resultField);
        panel.add(Box.createRigidArea(new Dimension(windowSize.width - 20, 15)));
        panel.add(calcButton);

        RefreshCalc();
        createFrame(panel).setVisible(true);
    }
}

package calculator;


import calculator.operation.AbstractOperation;
import calculator.operation.OperationNotSupportedException;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

public class Calculator implements AbstractEvaluator {
    protected final AbstractValueParser valueParser;
    protected final Map<String, AbstractOperation> operations;

    public Calculator(AbstractValueParser valueParser) {
        this.valueParser = valueParser;
        this.operations = new HashMap<String, AbstractOperation>();
        for (AbstractOperation operation : valueParser.getOperations()) {
            this.operations.put(operation.getName(), operation);
        }
    }

    protected String evaluateRPN(String expression)
            throws ParseValueException, DivisionByZeroException, OperationNotSupportedException {
        Stack<AbstractValue> stack = new Stack();
        Scanner scanner = new Scanner(expression);
        for (; scanner.hasNext(); ) {
            String token = scanner.next();
            if (operations.containsKey(token)) {
                if (stack.size() < 2) throw new ParseValueException();
                AbstractValue right = stack.pop();
                AbstractValue left = stack.pop();
                stack.push(operations.get(token).execute(left, right));
            } else stack.push(valueParser.parse(token));

        }
        if (stack.size() > 1 || stack.isEmpty())
            throw new ParseValueException();
        return stack.pop().toString();
    }

    int getOperationPriority(String op) {
        if (")".equals(op) || "(".equals(op)) return 0;
        return operations.get(op).getPriority();
    }

    // http://natalia.appmat.ru/c&c++/postfisso.html
    // TODO: clarify with lambdas
    protected String polishExpression(String expression) throws ParseValueException {
        String result = "";
        Stack<String> stack = new Stack();
        Scanner scanner = new Scanner(expression);
        for (; scanner.hasNext(); ) {
            String token = scanner.next();
            if ("(".equals(token)) {
                stack.push(token);
            } else if (")".equals(token)) {
                if (stack.isEmpty()) throw new ParseValueException();
                for (; !stack.isEmpty(); ) {
                    String token2 = stack.pop();
                    if ("(".equals(token2)) break;
                    result += token2 + " ";
                }
            } else if (operations.containsKey(token)) {
                int currentPriority = getOperationPriority(token);
                if (!stack.isEmpty() && !"(".equals(stack.peek()) &&
                        currentPriority <= getOperationPriority(stack.peek())) {
                    for (; !stack.isEmpty(); ) {
                        String token2 = stack.pop();
                        if (")".equals(token2)) break;
                        if (getOperationPriority(token2) < currentPriority) {
                            stack.push(token2);
                            break;
                        }
                        result += token2 + " ";
                    }
                }
                stack.push(token);
            } else result += token + " ";
        }
        if (stack.contains("(")) throw new ParseValueException();
        for (; !stack.isEmpty(); ) result += stack.pop() + " ";
        return result.trim();
    }


    @Override
    public String evaluate(String expression)
            throws ParseValueException, OperationNotSupportedException, DivisionByZeroException {
        return evaluateRPN(polishExpression(expression));
    }
}

package oop2;


/**
 * Created by Emil on 13.09.2016.
 */
public class Solver implements ISolver {
    static double eps = 0.00000001;

    protected final static boolean isSquareKnownCenter(Vector a, Vector center, Vector b){
        Vector aToC = a.sub(center);
        Vector bToC = b.sub(center);
        if (aToC.length() <= eps) return false;
        return aToC.length()==bToC.length() &&
                Math.abs(aToC.scalar(bToC)) <= eps;
    }

    public boolean isSquare(Vector p1, Vector p2, Vector p3) {
        return isSquareKnownCenter(p1, p2, p3) ||
                isSquareKnownCenter(p1, p3, p2) ||
                isSquareKnownCenter(p2, p1, p3);
    }

    public Vector tryGetForthSquarePoint(Vector p1, Vector p2, Vector p3) {
        if (isSquareKnownCenter(p1, p2, p3))
            return p1.sub(p2).add(p3.sub(p2)).add(p2);
        if (isSquareKnownCenter(p1, p3, p2))
            return p1.sub(p3).add(p2.sub(p3)).add(p3);
        if (isSquareKnownCenter(p2, p1, p3))
            return p2.sub(p1).add(p3.sub(p1)).add(p1);
        return null;
    }

    public double distanceFromPointToLine(Vector point, Line line) {
        Vector direction = line.getDirection();
        Vector height = line.start.sub(point);
        return height.cross(direction).length() / direction.length();
    }
}

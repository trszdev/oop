package oop2;

/**
 * Created by Emil on 13.09.2016.
 */
public interface ISolver {
    public boolean isSquare(Vector p1, Vector p2, Vector p3);
    public Vector tryGetForthSquarePoint(Vector p1, Vector p2, Vector p3);
    public double distanceFromPointToLine(Vector point, Line line);
}

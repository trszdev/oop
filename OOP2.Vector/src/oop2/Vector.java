package oop2;

/**
 * Created by Emil on 13.09.2016.
 */
public class Vector {
    public double a;
    public double b;
    public double c;

    public double[] getDimensions(){
        return new double[] {a,b,c};
    }

    public Vector(double a, double b, double c){
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Vector add(Vector other){
        return new Vector(other.a+this.a, other.b+this.b, other.c+this.c);
    }

    public Vector mult(double alpha){
        return new Vector(this.a*alpha, this.b*alpha, this.c*alpha);
    }

    public Vector sub(Vector other){
        return new Vector(-other.a+this.a, -other.b+this.b, -other.c+this.c);
    }

    public double scalar(Vector other){
        return other.a*this.a + other.b*this.b + other.c*this.c;
    }

    public Vector cross(Vector other){
        double newA = this.b*other.c-this.c*other.b;
        double newB = -(this.a*other.c-this.c*other.a);
        double newC = this.a*other.b-this.b*other.a;
        return new Vector(newA, newB, newC);

    }

    @Override
    public int hashCode() {
        return (int)(this.a*this.b*this.c);
    }

    @Override
    public boolean equals(Object otherObj){
        if (otherObj == null) return false;
        if (otherObj == this) return true;
        if (!(otherObj instanceof Vector))return false;
        Vector other = (Vector)otherObj;
        final double precision = 0.00000000000000001;
        return (Math.abs(this.a-other.a) <= precision &&
                Math.abs(this.b-other.b) <= precision &&
                Math.abs(this.c-other.c) <= precision);
    }
    public double length(){
        return Math.sqrt(a*a+b*b+c*c);
    }
}

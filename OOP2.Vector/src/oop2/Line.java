package oop2;

/**
 * Created by Emil on 13.09.2016.
 */
public class Line {
    public Line(Vector start, Vector end){
        this.start = start;
        this.end = end;
    }

    public Vector start;
    public Vector end;

    public Vector getDirection(){
        return end.sub(start);
    }
}

package oop2;

import org.junit.Test;

import javax.print.attribute.standard.MediaSize;

import static org.junit.Assert.*;

/**
 * Created by Emil on 15.09.2016.
 */
public class SolverTest {
    final ISolver Solver = new Solver();
    final double precision = 0.0000000001;

    private boolean isSquare(Vector[] points){
        return Solver.isSquare(points[0], points[1], points[2]);
    }

    private Vector tryGetForthSquarePoint(Vector[] points){
        return Solver.tryGetForthSquarePoint(points[0], points[1], points[2]);
    }

    private double distanceFromPointToLine(Vector[] points){
        return Solver.distanceFromPointToLine(points[0],
                new Line(points[1], points[2]));
    }

    @Test
    public void isSquare_sameDot() throws Exception {
        assertFalse(isSquare(new Vector[]{
                new Vector(0, 0, 0),
                new Vector(0, 0, 0),
                new Vector(0, 0, 0),
        }));
    }

    @Test
    public void isSquare_twoDots() throws Exception {
        assertFalse(isSquare(new Vector[]{
                new Vector(1, 0, 0),
                new Vector(1, 0, 0),
                new Vector(0, 0, 0),
        }));
    }

    @Test
    public void isSquare_threeDotsInOneLine() throws Exception {
        assertFalse(isSquare(new Vector[]{
                new Vector(-1, 0, 0),
                new Vector(2, 0, 0),
                new Vector(0, 0, 0),
        }));
    }

    @Test
    public void isSquare_threeDotsInOneLine2() throws Exception {
        assertFalse(isSquare(new Vector[]{
                new Vector(2, 2, 2),
                new Vector(1, 1, 1),
                new Vector(0, 0, 0),
        }));
    }

    @Test
    public void isSquare_common() throws Exception {
        assertTrue(isSquare(new Vector[]{
                new Vector(1, 1, 0),
                new Vector(1, 0, 0),
                new Vector(0, 0, 0),
        }));
    }


    @Test
    public void isSquare_rectangle1() throws Exception {
        assertFalse(isSquare(new Vector[]{
                new Vector(2, 2, 0),
                new Vector(1, 0, 0),
                new Vector(0, 0, 0),
        }));
    }


    @Test
    public void isSquare_rectangle2() throws Exception {
        assertFalse(isSquare(new Vector[]{
                new Vector(-2, -2, 0),
                new Vector(-2, 4, 0),
                new Vector(-2, 0, 0),
        }));
    }


    @Test
    public void tryGetForthSquarePoint_common() throws Exception {
        assertEquals(new Vector(0, 1, 0), tryGetForthSquarePoint(new Vector[] {
                new Vector(1, 0, 0),
                new Vector(0, 0, 0),
                new Vector(1, 1, 0),
        }));
    }


    @Test
    public void tryGetForthSquarePoint_common2() throws Exception {
        assertEquals(new Vector(-20, 20, 0), tryGetForthSquarePoint(new Vector[] {
                new Vector(-10, 10, 0),
                new Vector(-10, 20, 0),
                new Vector(-20, 10, 0),
        }));
    }


    @Test
    public void tryGetForthSquarePoint_rectangle() throws Exception {
        assertEquals(null, tryGetForthSquarePoint(new Vector[] {
                new Vector(10, 0, 0),
                new Vector(-10, 0, 0),
                new Vector(10, 10, 0),
        }));
    }


    @Test
    public void distanceFromPointToLine_common1() throws Exception {
        assertEquals(10, distanceFromPointToLine(new Vector[]{
            new Vector(0, 10, 0),
            new Vector(0, 0, 0),
            new Vector(10, 0, 0)
        }), precision);
    }


    @Test
    public void distanceFromPointToLine_common2() throws Exception {
        assertEquals(10, distanceFromPointToLine(new Vector[]{
                new Vector(5, 10, 0),
                new Vector(0, 0, 0),
                new Vector(10, 0, 0)
        }), precision);
    }


    @Test
    public void distanceFromPointToLine_common_outOfSegment() throws Exception {
        assertEquals(10, distanceFromPointToLine(new Vector[]{
                new Vector(5, 10, 0),
                new Vector(0, 0, 0),
                new Vector(10, 0, 0)
        }), precision);
    }

    @Test
    public void distanceFromPointToLine_pointInLine() throws Exception {
        assertEquals(0, distanceFromPointToLine(new Vector[]{
                new Vector(0, 10, 0),
                new Vector(0, 0, 0),
                new Vector(0, 3, 0)
        }), precision);
    }

    @Test
    public void distanceFromPointToLine_pointInLine2() throws Exception {
        assertEquals(0, distanceFromPointToLine(new Vector[]{
                new Vector(0, 10, 0),
                new Vector(0, 0, 0),
                new Vector(0, 11, 0)
        }), precision);
    }
}
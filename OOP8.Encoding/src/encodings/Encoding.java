package encodings;

/**
 * Created by Emil on 14.11.2016.
 */
public interface Encoding {
    String getName();
    boolean isValid(byte[] in);
    byte[] encode(String in); // from generic to utf8
    String decode(byte[] in) throws DecodingException; // from utf8 to generic
}

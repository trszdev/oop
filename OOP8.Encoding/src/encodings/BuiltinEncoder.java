package encodings;

import com.sun.media.jfxmedia.track.Track;

/**
 * Created by Emil on 22.11.2016.
 */
public class BuiltinEncoder implements AbstractEncoder {
    protected Encoding[] availableEncodings = new Encoding[] {
            Encodings.ascii,
            Encodings.cp866,
            Encodings.cp866,
            Encodings.cp1251,
            Encodings.utf8,
            Encodings.cp1254
    };

    @Override
    public Encoding determineEncoding(byte[] text) {
        for(Encoding encoding: availableEncodings){
            if (encoding.isValid(text)) return encoding;
        }
        return Encodings.empty;
    }

    @Override
    public Encoding[] getAvailableEncodings() {
        return availableEncodings;
    }
}

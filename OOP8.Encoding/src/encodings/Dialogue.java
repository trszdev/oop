package encodings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Emil on 21.11.2016.
 */
public final class Dialogue {
    public Dialogue(byte[] contents, AbstractEncoder encoder) {
        this.encoder = encoder;
        List<Byte> line = new ArrayList<>();
        for(byte symb: contents){
            if (symb=='\n'){ // ascii list
                byte[] toAdd = new byte[line.size()];
                for(int i=0; i< toAdd.length; i++) toAdd[i] = line.get(i);
                byteLines.add(toAdd);
                line = new ArrayList<>();
            }
            else line.add(symb);
        }
        if (line.size() > 0){
            byte[] toAdd = new byte[line.size()];
            for(int i=0; i< toAdd.length; i++) toAdd[i] = line.get(i);
            byteLines.add(toAdd);
        }
    }

    final AbstractEncoder encoder;
    final List<byte[]> byteLines = new ArrayList<>();

    public Encoding getFirstManEncoding(){
        if (byteLines.size() >= 1) {
            return encoder.determineEncoding(byteLines.get(0));
        }
        return Encodings.empty;
    }

    public Encoding getSecondManEncoding(){
        if (byteLines.size() >= 2) {
            return encoder.determineEncoding(byteLines.get(1));
        }
        return Encodings.empty;
    }

    public String[] getLines() throws DecodingException {
        List<String> result = new ArrayList<>(byteLines.size());
        boolean isOdd = true;
        Encoding first = getFirstManEncoding();
        Encoding second = getSecondManEncoding();
        for(byte[] line : byteLines){
            if (isOdd) result.add(first.decode(line));
            else result.add(second.decode(line));
            isOdd =! isOdd;
        }
        return result.toArray(new String[0]);
    }
}

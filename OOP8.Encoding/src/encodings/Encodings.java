package encodings;

/**
 * Created by Emil on 14.11.2016.
 */
public final class Encodings {
    public final static Encoding ascii = new BuiltinEncoding("ascii");
    public final static Encoding cp866 = new BuiltinEncoding("cp866");
    public final static Encoding cp1251 = new BuiltinEncoding("cp1251");
    public final static Encoding cp1254 = new BuiltinEncoding("cp1254");
    public final static Encoding utf8 = new BuiltinEncoding("utf8");
    public final static Encoding empty = new Encoding() {
        @Override
        public String getName() {
            return null;
        }

        @Override
        public boolean isValid(byte[] in) {
            return false;
        }

        @Override
        public byte[] encode(String in) {
            return new byte[0];
        }

        @Override
        public String decode(byte[] in) throws DecodingException {
            return null;
        }
    };
}

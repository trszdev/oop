package encodings.tests;

import encodings.Encodings;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * Created by Emil on 22.11.2016.
 */
public class Playground {
    public static void main(String[] args) throws UnsupportedEncodingException {
        byte[] strBytes = "Угу, да".getBytes("cp1251");
        System.out.println(Arrays.toString(strBytes));
        System.out.println(Encodings.cp866.isValid(strBytes));
    }
}

package encodings.tests;

import encodings.AbstractEncoder;
import encodings.BuiltinEncoder;
import encodings.DecodingException;
import encodings.Dialogue;

import static org.junit.Assert.*;

/**
 * Created by Emil on 22.11.2016.
 */

// test created with help of python2
public class DialogueTest {

    public void testDialogue(byte[] in, AbstractEncoder encoder,
                             String firstEncoding, String secondEncoding, String[] lines) throws Exception
    {
        Dialogue dialogue = new Dialogue(in, encoder);
        assertEquals(firstEncoding, dialogue.getFirstManEncoding().getName());
        assertEquals(secondEncoding, dialogue.getSecondManEncoding().getName());
        assertArrayEquals(lines, dialogue.getLines());
    }

    public void testDialogueWithBuiltinEncoder(byte[] in, String firstEncoding,
                             String secondEncoding, String[] lines) throws Exception
    {
        testDialogue(in, new BuiltinEncoder(), firstEncoding, secondEncoding, lines);
    }

    @org.junit.Test
    public void testEmpty() throws Exception {
        testDialogueWithBuiltinEncoder(new byte[0], null, null, new String[0]);
    }

    @org.junit.Test
    public void testSingleLineAsciiOnly1() throws Exception {
        testDialogueWithBuiltinEncoder(new byte[] {
                104, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100 //hello world
        }, "ascii", null, new String[]{
                "hello world"
        });
    }

    @org.junit.Test
    public void testSingleLineAsciiOnly2() throws Exception {
        testDialogueWithBuiltinEncoder(new byte[] {
                104, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100, 10 //hello world
        }, "ascii", null, new String[]{
                "hello world"
        });
    }

    @org.junit.Test
    public void testTwoLinesAsciiOnly1() throws Exception {
        testDialogueWithBuiltinEncoder(new byte[] {
             104, 101, 108, 108, 111, 32, 119, 111,
                114, 108, 100, 10, 104, 105, 32, 98, 111
                , 98
        }, "ascii", "ascii", new String[]{
                "hello world",
                "hi bob"
        });
    }

    @org.junit.Test
    public void testTwoLinesAsciiOnly2() throws Exception {
        testDialogueWithBuiltinEncoder(new byte[] {
                        119, 104, 97, 116, 32, 97, 98, 111, 117, 116, 32, 115, 111, 109, 101, 95, 115,
                112, 101, 99, 105, 97, 108, 32, 115, 121, 109, 98, 48, 49, 108, 115, 63, 63, 10,
                64, 35, 37, 94, 58, 119, 104, 105, 99, 104, 32, 111, 110, 101, 115, 38, 38, 41
        }, "ascii", "ascii", new String[]{
                "what about some_special symb01ls??",
                "@#%^:which ones&&)"
        });
    }

    @org.junit.Test
    public void testTwoLinesCP866_1() throws Exception {
        testDialogueWithBuiltinEncoder(new byte[] {
                -81, -32, -88, -94, -91, -30, 46, 32, -84, -21, 32, -89, -96, -90,
                -92, -96, -85, -88, -31, -20, 32, -30, -91, -95, -17,
                32, -91, -92, -88, -84, 32, 89, 111, 103, 104, 117, 114,
                116, 10, -128, -93, -96
        }, "cp866", "cp866", new String[]{
                "привет. мы заждались тебя едим Yoghurt",
                "Ага"
        });
    }

    @org.junit.Test
    public void testTwoLinesCP1254_1() throws Exception {
        testDialogueWithBuiltinEncoder(new byte[] {
                89, 117, 107, 97, 114, 100, 97, 32, 109, 97, 118, 105, 32, 103, -10, 107, 44, 32, 97, 115,
                97, -16, -3, 100, 97, 32, 121, 97, -16, -3, 122, 32, 121, 101, 114, 32, 121, 97, 114, 97,
                116, -3, 108, 100, -3, 107, 116, 97, 10, 105, 107, 105, 115, 105, 110, 105, 110, 32, 97, 114,
                97, 115, -3, 110, 100, 97, 32, 105, 110, 115, 97, 110,
                32, 111, -16, 108, 117, 32, 121, 97, 114, 97, 116, -3, 108, 109, -3, -2, 46
        }, "cp1254", "cp1254", new String[]{
                "Yukarda mavi gök, asağıda yağız yer yaratıldıkta",
                "ikisinin arasında insan oğlu yaratılmış."
        });
    }

    @org.junit.Test
    public void testTwoLinesCP1254vsCP1251_1() throws Exception {
        testDialogueWithBuiltinEncoder(new byte[] {
                89, 117, 107, 97, 114, 100, 97, 32, 109, 97, 118, 105, 32, 103, -10, 107, 44, 32, 97, 115,
                97, -16, -3, 100, 97, 32, 121, 97, -16, -3, 122,
                32, 121, 101, 114, 32, 121, 97, 114, 97, 116, -3, 108, 100, -3, 107, 116, 97, 10,
                -45, -29, -13, 44, 32, -28, -32,
        }, "cp1254", "cp1251", new String[]{
                "Yukarda mavi gök, asağıda yağız yer yaratıldıkta",
                "Угу, да"
        });
    }
}
package encodings;

/**
 * Created by Emil on 14.11.2016.
 */
public interface AbstractEncoder {
    Encoding determineEncoding(byte[] text);
    Encoding[] getAvailableEncodings();
}

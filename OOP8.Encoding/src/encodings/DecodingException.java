package encodings;

/**
 * Created by Emil on 21.11.2016.
 */
public class DecodingException extends Exception {
    public DecodingException(String encoding) {
        super(String.format("%s failed to decode", encoding));
    }
}

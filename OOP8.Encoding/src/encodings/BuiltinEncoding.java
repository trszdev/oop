package encodings;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * Created by Emil on 21.11.2016.
 */

// full list at https://docs.oracle.com/javase/8/docs/technotes/guides/intl/encoding.doc.html

public class BuiltinEncoding implements Encoding {
    protected String name;

    public BuiltinEncoding(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isValid(byte[] in) {
        try {
            return Arrays.equals(new String(in, name).getBytes(name), in);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String decode(byte[] in) throws DecodingException {
        try {
            return new String(in, name);
        } catch (UnsupportedEncodingException e) {
            throw new DecodingException(name);
        }
    }

    @Override
    public byte[] encode(String in) {
        try {
            return in.getBytes(getName());
        }
        catch (UnsupportedEncodingException e){
            throw new RuntimeException(e);
        }
    }
}

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Emil on 17.09.2016.
 */
public class Vector2Test {
    final double precision = 0.00000000001;

    @Test
    public void scalar_common() throws Exception {
        assertEquals(5, new Vector2(1, 1).scalar(new Vector2(2, 3)), precision);
    }

    @Test
    public void scalar_zero() throws Exception {
        assertEquals(0, new Vector2(0, 0).scalar(new Vector2(2, 3)), precision);
    }

    @Test
    public void len_common1() throws Exception {
        assertEquals(1, new Vector2(1, 0).len(), precision);
    }

    @Test
    public void len_common2() throws Exception {
        assertEquals(2, new Vector2(0, -2).len(), precision);
    }

    @Test
    public void len_common3() throws Exception {
        assertEquals(4 * Math.sqrt(2), new Vector2(-4, -4).len(), precision);
    }

    @Test
    public void multiply() throws Exception {
        AbstractVector result = new Vector2(1, 2).multiply(3);
        assertTrue(result instanceof Vector2);
        Vector2 vec = (Vector2) result;
        assertEquals(3, vec.getA(), precision);
        assertEquals(6, vec.getB(), precision);
    }

    @Test
    public void add() throws Exception {
        AbstractVector result = new Vector2(1, 1).add(new Vector2(1, 2));
        assertTrue(result instanceof Vector2);
        Vector2 vec = (Vector2) result;
        assertEquals(2, vec.getA(), precision);
        assertEquals(3, vec.getB(), precision);
    }

    @Test
    public void sub() throws Exception {
        AbstractVector result = new Vector2(1, 1).sub(new Vector2(1, 2));
        assertTrue(result instanceof Vector2);
        Vector2 vec = (Vector2) result;
        assertEquals(0, vec.getA(), precision);
        assertEquals(-1, vec.getB(), precision);
    }

}
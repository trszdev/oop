import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Emil on 17.09.2016.
 */
public class SegmentTest {
    final double precision = 0.00000000001;

    private Segment<Vector2> createSegment2(double... coords) {
        return new Segment<Vector2>(new Vector2(coords[0], coords[1]),
                new Vector2(coords[2], coords[3]));
    }

    private Segment<Vector3> createSegment3(double... coords) {
        return new Segment<Vector3>(new Vector3(coords[0], coords[1], coords[2]),
                new Vector3(coords[3], coords[4], coords[5]));
    }

    @Test
    public void len_sameDot_vec2_1() throws Exception {
        assertEquals(0, createSegment2(1, 2, 1, 2).len(), precision);
    }

    @Test
    public void len_sameDot_vec2_2() throws Exception {
        assertEquals(0, createSegment2(-1, 2, -1, 2).len(), precision);
    }


    @Test
    public void len_sameDot_vec3() throws Exception {
        assertEquals(0, createSegment3(1, 1, 2, 1, 1, 2).len(), precision);
    }


    @Test
    public void len_common_vec3_1() throws Exception {
        assertEquals(10, createSegment3(0, 0, 0, 10, 0, 0).len(), precision);
    }


    @Test
    public void len_common_vec3_2() throws Exception {
        assertEquals(10 * Math.sqrt(2), createSegment3(0, 0, 0, 10, 10, 0).len(), precision);
    }

    @Test
    public void len_common_vec2_1() throws Exception {
        assertEquals(10, createSegment2(0, 0, 10, 0).len(), precision);
    }

    @Test
    public void len_common_vec2_2() throws Exception {
        assertEquals(20 * Math.sqrt(2), createSegment2(-10, -10, 10, 10).len(), precision);
    }

    @Test
    public void distanceTo_dotInSegment_vec2_1() throws Exception {
        assertEquals(0, createSegment2(0, 0, 1, 0).distanceTo(new Vector2(0.5, 0)), precision);
    }

    @Test
    public void distanceTo_dotInSegment_vec2_2() throws Exception {
        assertEquals(0, createSegment2(-2, -2, 1, 1).distanceTo(new Vector2(0.5, 0.5)), precision);
    }

    @Test
    public void distanceTo_dotInSegment_vec3() throws Exception {
        assertEquals(0, createSegment3(-2, -2, 0, 1, 1, 0).distanceTo(new Vector3(0.5, 0.5, 0)), precision);
    }


    @Test
    public void distanceTo_common_vec2_1() throws Exception {
        assertEquals(Math.sqrt(2),
                createSegment2(-2, -2, 1, 1).distanceTo(new Vector2(2, 2)), precision);
    }

    @Test
    public void distanceTo_common_vec2_2() throws Exception {
        assertEquals(Math.sqrt(2),
                createSegment2(-2, -2, 1, 1).distanceTo(new Vector2(-3, -3)), precision);
    }

    @Test
    public void distanceTo_common_vec3_1() throws Exception {
        assertEquals(Math.sqrt(3),
                createSegment3(-2, -2, -2, 1, 1, 1).distanceTo(new Vector3(-3, -3, -3)), precision);
    }

    @Test
    public void distanceTo_common_vec3_2() throws Exception {
        assertEquals(2 * Math.sqrt(3),
                createSegment3(-2, -2, -2, 1, 1, 1).distanceTo(new Vector3(3, 3, 3)), precision);
    }

    @Test
    public void distanceTo_common_vec3_3() throws Exception {
        assertEquals(2.5 * Math.sqrt(2),
                createSegment3(-2, -2, 3, 3, 3, 3).distanceTo(new Vector3(-2, 3, 3)), precision);
    }

    @Test
    public void distanceTo_common_vec2_3() throws Exception {
        assertEquals(1, createSegment2(-2, 0, 1, 0).distanceTo(new Vector2(0, 1)), precision);
    }


    @Test
    public void distanceTo_common_vec2_4() throws Exception {
        assertEquals(1, createSegment2(-2, 0, 1, 0).distanceTo(new Vector2(0, -1)), precision);
    }
}
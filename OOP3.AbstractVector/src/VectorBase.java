/**
 * Created by Emil on 20.09.2016.
 */
public abstract class VectorBase implements AbstractVector {
    public abstract double scalar(AbstractVector v);

    public abstract double len();

    public abstract AbstractVector multiply(double factor);

    public abstract AbstractVector add(AbstractVector v);

    public abstract AbstractVector sub(AbstractVector v);

    // https://en.wikipedia.org/wiki/Vector_projection
    public AbstractVector projection(AbstractVector v) {
        return v.multiply(scalar(v) / v.len());
    }
}

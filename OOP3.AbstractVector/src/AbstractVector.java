/**
 * Created by Emil on 17.09.2016.
 */
interface AbstractVector {
    double scalar(AbstractVector v);

    double len();

    AbstractVector multiply(double factor);

    AbstractVector add(AbstractVector v);

    AbstractVector projection(AbstractVector v);

    AbstractVector sub(AbstractVector v);
}

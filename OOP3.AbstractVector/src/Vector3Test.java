import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Emil on 17.09.2016.
 */
public class Vector3Test {
    final double precision = 0.00000000001;

    @org.junit.Test
    public void scalar() throws Exception {
        assertEquals(14, new Vector3(1, 2, 3).scalar(new Vector3(1, 2, 3)), precision);
    }

    @org.junit.Test
    public void len() throws Exception {
        assertEquals(10, new Vector3(10, 0, 0).len(), precision);
    }

    @org.junit.Test
    public void multiply() throws Exception {
        AbstractVector result = new Vector3(1, 2, 3).multiply(3);
        assertTrue(result instanceof Vector3);
        Vector3 vec = (Vector3) result;
        assertEquals(3, vec.getA(), precision);
        assertEquals(6, vec.getB(), precision);
        assertEquals(9, vec.getC(), precision);
    }

    @org.junit.Test
    public void add() throws Exception {
        AbstractVector result = new Vector3(1, 2, 3).add(new Vector3(-1, -1, 2));
        assertTrue(result instanceof Vector3);
        Vector3 vec = (Vector3) result;
        assertEquals(0, vec.getA(), precision);
        assertEquals(1, vec.getB(), precision);
        assertEquals(5, vec.getC(), precision);
    }

    @org.junit.Test
    public void sub() throws Exception {
        AbstractVector result = new Vector3(1, 2, 3).sub(new Vector3(-1, -1, 2));
        assertTrue(result instanceof Vector3);
        Vector3 vec = (Vector3) result;
        assertEquals(2, vec.getA(), precision);
        assertEquals(3, vec.getB(), precision);
        assertEquals(1, vec.getC(), precision);
    }

}
/**
 * Created by Emil on 17.09.2016.
 */
public class Segment<TVector extends AbstractVector> {
    protected TVector start;
    protected TVector end;

    public Segment(TVector start, TVector end) {
        this.start = start;
        this.end = end;
    }

    public TVector getStart() {
        return start;
    }

    public TVector getEnd() {
        return end;
    }

    public double len() {
        return end.sub(start).len();
    }

    // http://algolist.manual.ru/maths/geom/distance/pointline.php
    public double distanceTo(TVector point) {
        AbstractVector direction = end.sub(start);
        AbstractVector pointToStart = point.sub(start);
        AbstractVector pointToEnd = point.sub(end);
        double c1 = direction.scalar(pointToStart);
        double c2 = direction.scalar(direction);
        if (c1 <= 0) return pointToStart.len();
        if (c2 <= c1) return pointToEnd.len();
        return point.sub(start.add(direction.multiply(c1 / c2))).len();
    }
}

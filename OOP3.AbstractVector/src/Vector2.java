/**
 * Created by Emil on 17.09.2016.
 */
public class Vector2 extends VectorBase {
    protected double a;
    protected double b;


    public Vector2(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    @Override
    public double scalar(AbstractVector v) {
        Vector2 vec = (Vector2) v;
        return vec.a * a + vec.b * b;
    }

    @Override
    public double len() {
        return Math.sqrt(a * a + b * b);
    }

    @Override
    public AbstractVector multiply(double factor) {
        return new Vector2(a * factor, b * factor);
    }

    @Override
    public AbstractVector add(AbstractVector v) {
        Vector2 vec = (Vector2) v;
        return new Vector2(vec.a + a, vec.b + b);
    }

    @Override
    public AbstractVector sub(AbstractVector v) {
        Vector2 vec = (Vector2) v;
        return new Vector2(-vec.a + a, -vec.b + b);
    }
}

/**
 * Created by Emil on 17.09.2016.
 * Immutable 3d vector
 */
public class Vector3 extends VectorBase {
    protected double a;
    protected double b;
    protected double c;

    public Vector3(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    @Override
    public double scalar(AbstractVector v) {
        Vector3 vec = (Vector3) v;
        return vec.a * a + vec.b * b + vec.c * c;
    }

    @Override
    public double len() {
        return Math.sqrt(a * a + b * b + c * c);
    }

    @Override
    public AbstractVector multiply(double factor) {
        return new Vector3(a * factor, b * factor, c * factor);
    }

    @Override
    public AbstractVector add(AbstractVector v) {
        Vector3 vec = (Vector3) v;
        return new Vector3(vec.a + a, vec.b + b, vec.c + c);
    }

    @Override
    public AbstractVector sub(AbstractVector v) {
        Vector3 vec = (Vector3) v;
        return new Vector3(-vec.a + a, -vec.b + b, -vec.c + c);
    }
}

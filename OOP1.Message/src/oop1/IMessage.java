package oop1;

/**
 * Created by Emil on 12.09.2016.
 */
public interface IMessage {
    // возвращает количество слов
    public int countWords();

    // возвращает переверную строку
    public String reverse();

    // возвращает кол-во вхождений символа в строку
    public int count(char c);

    // проверяет правильность расстановки скобок []{}()<> в строке
    public boolean isValid();

    // увеличивает код каждого символа на shift
    public String encode(int shift);
}

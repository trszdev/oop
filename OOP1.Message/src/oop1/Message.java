package oop1;


import java.util.Stack;

/**
 * Created by Emil on 12.09.2016.
 */
public class Message implements IMessage {
    public String toString(){
        return "'"+this.content+"'";
    }


    private String content;
    public Message(String content){
        boolean isNullOrEmpty = content == null || content.trim().length() == 0;
        if (isNullOrEmpty)
            throw new IllegalArgumentException("ща уроню на тебя исходник IBM dataquant, шол вон");
        this.content = content;
    }

    public int countWords() {
        return content.trim().split(" ").length;
    }

    public String reverse() {
        String result = "";
        for(int i=this.content.length()-1;i>=0;i--){
            result += this.content.charAt(i);
        }
        return result;
    }

    public int count(char c) {
        int result = 0;
        for(char ch : this.content.toCharArray()){
            if (ch==c) result++;
        }
        return result;
    }

    // TODO
    public boolean isValid() {
        Stack<Character> stack = new Stack<Character>();
        for(char ch : this.content.toCharArray()){
            if ("[({<".indexOf(ch)!=-1){
                stack.push(new Character(ch));
            }
            else if ("])}>".indexOf(ch)!=-1){
                if (stack.size()==0) return false;
                char popped = stack.pop();
                if (popped == '{' && ch != '}') return  false;
                if (popped == '<' && ch != '>') return  false;
                if (popped == '(' && ch != ')') return  false;
                if (popped == '[' && ch != ']') return  false;
            }
        }
        return stack.size()==0;
    }

    public String encode(int shift) {
        String result = "";
        for(char ch : this.content.toCharArray()){
            result += (char)(ch+shift);
        }
        return result;
    }
}

package oop1;

import java.*;


public class Main {

    public static void testIsValid(IMessage message, boolean expected){
        if (message.isValid()!=expected)
            System.out.println("Error isValid on " + message.toString());
    }

    public static void testCountWords(IMessage message, int expected){
        if (message.countWords()!=expected)
            System.out.println("Error countWords on " + message.toString());
    }

    public static void testReverse(IMessage message, String expected){
        if (!message.reverse().equals(expected))
            System.out.println("Error countWords on " + message.toString());
    }

    public static void testCountChar(IMessage message, char charToFind, int expected){
        if (message.count(charToFind)!=expected)
            System.out.println("Error count on " + message.toString());
    }


    public static void testEncode(IMessage message, int shift, String expected){
        if (!message.encode(shift).equals(expected))
            System.out.println("Error encode on " + message.toString());
    }

    public static void main(String[] args) {
        testIsValid(new Message("()()()[()]"), true);
        testIsValid(new Message(")()()[()]"), false);
        testIsValid(new Message("()()()[()"), false);
        testIsValid(new Message("()()()([)]"), false);
        testIsValid(new Message("([[[([])]]])"), true);
        testIsValid(new Message("([[[()]]])"), true);
        testIsValid(new Message("Hello world"), true);
        testCountWords(new Message("Hi"), 1);
        testCountWords(new Message("    Hi   "), 1);
        testCountWords(new Message("Hi bob   "), 2);
        testReverse(new Message("Hello world"), "dlrow olleH");
        testCountChar(new Message("alice bob alice hsyt @A"), 'a', 2);
        testEncode(new Message("AAAAAB"), 1, "BBBBBC");
        System.out.println("Tests finished");
    }
}

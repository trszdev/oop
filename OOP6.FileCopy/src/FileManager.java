import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Emil on 29.09.2016.
 */
public class FileManager extends FileManagerBase {

    // TODO: block copy
    @Override
    public FileCopyResult copyFile(FileInputStream in, FileOutputStream out) throws Exception {
        MutableFileCopyResult result = new MutableFileCopyResult();
        result.size = in.available();
        result.copied = 0;
        result.isOk = true;
        result.filename = getFilename(out);
        result.runnable = () -> {
            try {
                for (int i = -1; (i = in.read()) != -1; ) {
                    out.write(i);
                    result.copied++;
                    result.copyListeners.forEach(e -> e.onCopy(result));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
        return result;
    }
}

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Emil on 29.09.2016.
 */
public interface AbstractFileManager {
    FileCopyResult copyFile(FileInputStream in, FileOutputStream out) throws IOException, NoSuchFieldException, IllegalAccessException, Exception;

    FileOutputStream openWrite(String path) throws IOException;
}

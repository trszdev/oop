import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Emil on 01.10.2016.
 */
public abstract class FileManagerBase implements AbstractFileManager {
    protected final int chunkSize = 2;

    @Override
    public abstract FileCopyResult copyFile(FileInputStream in, FileOutputStream out)
            throws Exception;

    // http://stackoverflow.com/questions/4930111/get-file-name-from-fileoutputstream
    protected final String getFilename(FileOutputStream out)
            throws NoSuchFieldException, IllegalAccessException {
        Field pathField = FileOutputStream.class.getDeclaredField("path");
        pathField.setAccessible(true);
        return (String) pathField.get(out);
    }


    @Override
    public FileOutputStream openWrite(String path) throws IOException {
        int dotPosition = path.lastIndexOf('.');
        String extension = dotPosition == -1 ? "" : path.substring(dotPosition, path.length());
        String baseName = dotPosition == -1 ? path : path.substring(0, dotPosition);
        String newFileName = path;
        for (int i = 1; new File(newFileName).exists(); i++)
            newFileName = String.format("%s-Copy(%d)%s", baseName, i, extension);

        Files.createDirectories(Paths.get(newFileName).getParent());
        return new FileOutputStream(newFileName, false);
    }
}

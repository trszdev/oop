/**
 * Created by Emil on 29.09.2016.
 */
public interface FileCopyResult extends Runnable {
    boolean isOk();

    int getSizeBytes();

    int getCopiedBytes();

    String getFileName();

    void addCopyListener(FileCopyEventListener eventListener);
}

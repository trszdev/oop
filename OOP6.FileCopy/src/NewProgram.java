import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by Emil on 07.11.2016.
 */
public class NewProgram {
    static void main(String inputPath, String outputPath, int threadsAmount) throws Exception {
        System.out.println("Trying to make "+threadsAmount+" copies of '"
                +inputPath+"' in '"+outputPath+"'");
        File outFile = new File(outputPath);

        long offset = outFile.length(); // если хотим дополнить
        RandomAccessFile out = new RandomAccessFile(outFile, "rw");
        System.out.println("Base offset: " + offset);
        byte[] in = Files.readAllBytes(Paths.get(inputPath));
        Thread[] threads = new Thread[threadsAmount];
        for(int i=0;i<threadsAmount;i++)
        {
            final int k = i; // ????????????? пришлось
            threads[i] = new Thread(() -> {
                try {
                    synchronized (out) {
                        out.seek(offset + k * in.length);
                        out.write(in);
                    }
                    System.out.println("Writing "+(k+1)+"th block on offset "+(k*in.length));
                } catch(Exception e){
                    e.printStackTrace();
                }
            });
            threads[i].start();
        }

        for(int i = 0; i < threadsAmount; i++)
            threads[i].join();
    }

    public static void main(String[] args) throws Exception {
        main("test.bat", "Ncopiesoftestbat", 5);
    }
}

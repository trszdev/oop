import java.util.ArrayList;
import java.util.List;

/**
 * Created by Emil on 01.10.2016.
 */
public class MutableFileCopyResult implements FileCopyResult {
    public final List<FileCopyEventListener> copyListeners
            = new ArrayList<>();
    public boolean isOk = true;
    public int size = 0;
    public int copied = 0;
    public Runnable runnable;
    String filename;


    @Override
    public boolean isOk() {
        return isOk;
    }

    @Override
    public int getSizeBytes() {
        return size;
    }

    @Override
    public int getCopiedBytes() {
        return copied;
    }

    @Override
    public String getFileName() {
        return filename;
    }

    @Override
    public void addCopyListener(FileCopyEventListener eventListener) {
        copyListeners.add(eventListener);
    }

    @Override
    public void run() {
        runnable.run();
    }
}

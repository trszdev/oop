import java.util.EventListener;

/**
 * Created by Emil on 02.10.2016.
 */
@FunctionalInterface
public interface FileCopyEventListener extends EventListener {
    void onCopy(FileCopyResult copyResult);
}

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Emil on 01.10.2016.
 */
class ProgramArgs {
    public String inFilename;
    public String outFilename;
    public boolean isSilent = false;
    public int copies = 2;
    public int threads = 2;
}

// async
public class Program {
    static void print(String line) {
        System.out.println(line);
    }

    static final void checkArgs(ProgramArgs args) {
        if (args.inFilename == null)
            throw new IllegalArgumentException("Input filename is required");
        if (args.outFilename == null)
            throw new IllegalArgumentException("Output filename is required");
        if (args.copies <= 0)
            throw new IllegalArgumentException("Copies must be >= 1");
        if (args.threads <= 0)
            throw new IllegalArgumentException("Threads must be >= 1");
    }

    static String getNormalPath(String path) {
        return new File(path).toPath().toAbsolutePath().normalize().toString();
    }


    public static final void main(ProgramArgs args) throws Exception {
        checkArgs(args);
        ExecutorService executorService = Executors.newFixedThreadPool(args.threads);
        //executorService = Executors.newSingleThreadExecutor();
        String inPath = getNormalPath(args.inFilename);
        String outPath = getNormalPath(args.outFilename);
        if (!args.isSilent)
            print("copying \"" + inPath + "\" to \"" + outPath + "\"");
        AbstractFileManager fileManager = new FileManager();
        List<Callable<Void>> tasks = new ArrayList<>();
        for (int i = 1; i <= args.copies; i++) {
            FileOutputStream out = fileManager.openWrite(outPath);
            FileInputStream in = new FileInputStream(inPath);
            FileCopyResult copyResult = fileManager.copyFile(in, out);
            copyResult.addCopyListener(e -> {
                if (!args.isSilent)
                    print(String.format("%s [%.2f%%]", e.getFileName(), 100.0 * e.getCopiedBytes() / e.getSizeBytes()));
            });
            //copyResult.run();
            tasks.add(() -> {
                copyResult.run();
                return null;
            });
        }
        executorService.invokeAll(tasks);
        executorService.shutdown();
        print("i think ive done");
    }


    public static final void main(String[] arguments) throws Exception {
        List args = Arrays.asList(arguments);
        if (args.contains("-h") || args.contains("--help") || args.size() == 0) {
            print("\tthis tool creates many file copies with unique name");
            print("");
            print("\tusage: java FileCopy.java [--in=filename] [--out=filename]" +
                    " [--silent] [--copies=2] [--threads=2] [--help]");
            return;
        }
        ProgramArgs programArgs = new ProgramArgs();
        for (String arg : arguments) {
            if (arg.equals("--silent")) programArgs.isSilent = true;
            if (arg.startsWith("--in="))
                programArgs.inFilename = arg.substring("--in=".length(), arg.length());
            if (arg.startsWith("--out="))
                programArgs.outFilename = arg.substring("--out=".length(), arg.length());
            if (arg.startsWith("--copies="))
                programArgs.copies = Integer.parseInt(arg.substring("--copies=".length(), arg.length()));
            if (arg.startsWith("--threads="))
                programArgs.threads = Integer.parseInt(arg.substring("--threads=".length(), arg.length()));
        }
        main(programArgs);
    }
}

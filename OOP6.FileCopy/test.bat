@echo off
setlocal

rd /q /s out\test & :: delete previous test results
cd out\production\OOP6.FileCopy
java Program --in=..\..\..\test.bat --out=..\..\test\test.bat --copies=10 --threads=10

cd ..\..\..\ & :: goto current dir again
fc /b test.bat out\test\test.bat

for %%N in (1 2 3 4 5 6 7 8 9) do (
    fc /b test.bat "out\test\test-Copy(%%N).bat"
)

endlocal


:: some padding for testing..)
::
::
::
::
::
::
::
::
::
::
::
::
::
::
::
::
::
::
::
::
::
::
::
::
:: nice
